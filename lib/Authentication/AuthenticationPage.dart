import 'package:flutter/material.dart';
import 'package:movie_suggestion_app/Authentication/LoginPage.dart';
import 'package:movie_suggestion_app/Authentication/RegisterPage.dart';

import 'RequestPasswordReset.dart';

enum PAGES { LOGIN, REGISTER, PASSWORD_RESET }

class AuthenticationPage extends StatefulWidget {
  AuthenticationPage({Key? key}) : super(key: key);

  @override
  _AuthenticationPageState createState() => _AuthenticationPageState();
}

class _AuthenticationPageState extends State<AuthenticationPage> {
  var toShow = PAGES.LOGIN;

  void show(page) {
    setState(() {
      toShow = page;
    });
  }

  @override
  Widget build(BuildContext context) {
    var child;
    switch (toShow) {
      case PAGES.REGISTER:
        child = RegisterPage(callback: () {
          show(PAGES.LOGIN);
        });
        break;
      case PAGES.PASSWORD_RESET:
        child = RequestPasswordReset(callback: () {
          show(PAGES.LOGIN);
        });
        break;
      default:
        child = LoginPage(registerCallback: () {
          show(PAGES.REGISTER);
        }, passwordResetCallback: () {
          show(PAGES.PASSWORD_RESET);
        });
    }

    return child;
  }
}
