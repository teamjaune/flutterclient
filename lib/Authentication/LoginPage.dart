import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:movie_suggestion_app/Config/Environment.dart';
import 'package:movie_suggestion_app/internationalization/ErrorLocalizations.dart';
import 'package:movie_suggestion_app/internationalization/TJLocalizations.dart';
import 'package:movie_suggestion_app/utilities/AppState.dart';
import 'package:movie_suggestion_app/widgets/TextDivider.dart';
import 'package:provider/src/provider.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

String firebaseErrorTranslator(BuildContext context, String firebaseError) {
  if (firebaseError == 'invalid-email') {
    return ErrorLocalizations.of(context)!.getElement("invalid-email");
  } else if (firebaseError == 'user-disabled') {
    return ErrorLocalizations.of(context)!.getElement("user-disabled");
  } else if (firebaseError == 'user-not-found') {
    return ErrorLocalizations.of(context)!.getElement("user-not-found");
  } else if (firebaseError == 'wrong-password') {
    return ErrorLocalizations.of(context)!.getElement("wrong-password");
  }
  return ErrorLocalizations.of(context)!.getElement("operation-not-allowed");
}

class LoginPage extends StatefulWidget {
  final Function registerCallback;
  final Function passwordResetCallback;

  LoginPage(
      {required this.registerCallback,
      required this.passwordResetCallback,
      Key? key})
      : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final emailController = TextEditingController();
  final passController = TextEditingController();
  final serverController = TextEditingController();
  final double radius = 32;
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  String errorMessage = "";

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    emailController.dispose();
    passController.dispose();
    super.dispose();
  }

  void startFirebaseAuth(
      BuildContext context, String email, String password) async {
    if ("" == email || "" == password) {
      setState(() {
        errorMessage = ErrorLocalizations.of(context)!
            .getElement("empty-email-or-password");
      });
      return;
    }
    try {
      await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);
    } on FirebaseAuthException catch (e) {
      setState(() {
        errorMessage = firebaseErrorTranslator(context, e.code);
      });
    } catch (exception, stackTrace) {
      if (Environment().config.reportErrors)
        await Sentry.captureException(
          exception,
          stackTrace: stackTrace,
        );
      else
        print(exception);
    }
  }

  Future<UserCredential> signInWithGoogle(BuildContext context) async {
    context.read<AppState>().serverAddress = serverController.value.text;

    // Trigger the authentication flow
    final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

    // Obtain the auth details from the request
    final GoogleSignInAuthentication? googleAuth =
        await googleUser?.authentication;

    // Create a new credential
    final credential = GoogleAuthProvider.credential(
      accessToken: googleAuth?.accessToken,
      idToken: googleAuth?.idToken,
    );
    // Once signed in, return the UserCredential
    return await FirebaseAuth.instance.signInWithCredential(credential);
  }

  @override
  Widget build(BuildContext context) {
    var accentColor = Colors.black;
    var inputFieldTextStyle = TextStyle(color: accentColor);
    var borderSettings = OutlineInputBorder(
        borderSide: BorderSide(width: 2, color: accentColor),
        borderRadius: BorderRadius.all(Radius.circular(radius)));
    var focusedBorderSettings = OutlineInputBorder(
        borderSide: BorderSide(width: 2, color: accentColor),
        borderRadius: BorderRadius.all(Radius.circular(radius)));

    final emailField = TextField(
        controller: emailController,
        obscureText: false,
        style: style,
        decoration: InputDecoration(
          fillColor: accentColor,
          prefixIcon: Icon(
            Icons.person_outline,
            color: accentColor,
          ),
          hintStyle: inputFieldTextStyle,
          labelStyle: inputFieldTextStyle,
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          labelText: TJLocalizations.of(context)?.getElement("LOGIN_EMAIL"),
          focusedBorder: focusedBorderSettings,
          enabledBorder: focusedBorderSettings,
          border: borderSettings,
        ));

    final passwordField = TextField(
        controller: passController,
        obscureText: true,
        style: style,
        decoration: InputDecoration(
            prefixIcon: Icon(Icons.lock_outline, color: accentColor),
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            labelText:
                TJLocalizations.of(context)?.getElement("LOGIN_PASSWORD"),
            labelStyle: inputFieldTextStyle,
            focusedBorder: focusedBorderSettings,
            enabledBorder: focusedBorderSettings,
            border: borderSettings));
    serverController.text = context.read<AppState>().serverAddress;
    final serverAddress = TextFormField(
        initialValue: context.read<AppState>().serverAddress,
        onFieldSubmitted: (value) {
          context.read<AppState>().serverAddress = value;
        },
        style: style,
        decoration: InputDecoration(
            prefixIcon: Icon(Icons.location_city, color: accentColor),
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            labelText: "Server",
            labelStyle: inputFieldTextStyle,
            focusedBorder: focusedBorderSettings,
            enabledBorder: focusedBorderSettings,
            border: borderSettings));
    final loginButon = Material(
      elevation: 5.0,
      color: accentColor,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width / 2,
        onPressed: () {
          startFirebaseAuth(context, emailController.text, passController.text);
        },
        child: Text(
            TJLocalizations.of(context)?.getElement("LOGIN_BUTTON") ?? "",
            textAlign: TextAlign.center,
            style: style.copyWith(color: Colors.white)),
      ),
    );

    return Scaffold(
        body: Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
        begin: Alignment.topRight,
        end: Alignment.bottomLeft,
        colors: [
          Colors.yellow[50]!,
          Colors.red[100]!,
        ],
      )),
      child: Center(
          child: SingleChildScrollView(
              child: Padding(
                  padding: const EdgeInsets.all(36.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        TJLocalizations.of(context)
                                ?.getElement("LOGIN_TITLE") ??
                            "",
                        style: TextStyle(
                            fontSize: 32, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 16.0),
                      SizedBox(
                        height: 32.0,
                        child: Text(
                          errorMessage,
                          style: TextStyle(
                              color: Colors.red, fontWeight: FontWeight.bold),
                        ),
                      ),
                      serverAddress,
                      SizedBox(height: 16.0),
                      emailField,
                      SizedBox(height: 16.0),
                      passwordField,
                      SizedBox(height: 16.0),
                      loginButon,
                      SizedBox(height: 16.0),
                      InkWell(
                          onTap: () {
                            widget.passwordResetCallback();
                          },
                          child: Text(
                            TJLocalizations.of(context)
                                    ?.getElement("LOGIN_FORGOT_PASSWORD") ??
                                "",
                            style: new TextStyle(
                                color: Colors.blue,
                                fontWeight: FontWeight.bold),
                          )),
                      SizedBox(height: 16.0),
                      InkWell(
                          onTap: () {
                            widget.registerCallback();
                          },
                          child: Text(
                            TJLocalizations.of(context)
                                    ?.getElement("LOGIN_CREATE_ACCOUNT") ??
                                "",
                            style: new TextStyle(
                                color: Colors.blue,
                                fontWeight: FontWeight.bold),
                          )),
                      SizedBox(height: 16.0),
                      TextDivider(TJLocalizations.of(context)
                              ?.getElement("LOGIN_DIVIDER_OR") ??
                          ""),
                      SizedBox(height: 16.0),
                      SizedBox(
                        child: SignInButton(
                          Buttons.Google,
                          text: TJLocalizations.of(context)
                                  ?.getElement("LOGIN_WITH_GOOGLE") ??
                              "",
                          onPressed: () {
                            signInWithGoogle(context);
                          },
                        ),
                      ),
                    ],
                  )))),
    ));
  }
}
