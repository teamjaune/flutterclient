import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:movie_suggestion_app/Config/Environment.dart';
import 'package:movie_suggestion_app/internationalization/TJLocalizations.dart';
import 'package:movie_suggestion_app/utilities/Pair.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

Pair firebaseErrorTranslator(String firebaseError) {
  if (firebaseError == 'invalid-email') {
    return new Pair(true, "Provided email is badly formated");
  } else if (firebaseError == 'user-not-found') {
    return new Pair(false, "An email will be sent if this account exists");
  }
  return new Pair(true, "Authentication failed, contact an administrator");
}

class RequestPasswordReset extends StatefulWidget {
  final Function callback;

  RequestPasswordReset({required this.callback, Key? key}) : super(key: key);

  @override
  _RequestPasswordResetPageState createState() =>
      _RequestPasswordResetPageState();
}

class _RequestPasswordResetPageState extends State<RequestPasswordReset> {
  final emailController = TextEditingController();

  final double radius = 32;
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 16.0);
  String errorMessage = "";
  bool isError = true;

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    emailController.dispose();
    super.dispose();
  }

  void resetFirebasePassword(String email) async {
    if ("" == email) {
      setState(() {
        errorMessage = "Email field cannot be empty";
      });
      return;
    }
    try {
      await FirebaseAuth.instance.sendPasswordResetEmail(email: email);
      setState(() {
        var result = firebaseErrorTranslator("user-not-found");
        errorMessage = result.right;
        isError = result.left;
      });
    } on FirebaseAuthException catch (e) {
      setState(() {
        var result = firebaseErrorTranslator(e.code);
        errorMessage = result.right;
        isError = result.left;
      });
    } catch (exception, stackTrace) {
      if (Environment().config.reportErrors)
        await Sentry.captureException(
          exception,
          stackTrace: stackTrace,
        );
      else
        print(exception);
    }
  }

  @override
  Widget build(BuildContext context) {
    var accentColor = Colors.black;
    var inputFieldTextStyle = TextStyle(color: accentColor);
    var borderSettings = OutlineInputBorder(
        borderSide: BorderSide(width: 2, color: accentColor),
        borderRadius: BorderRadius.all(Radius.circular(radius)));
    var focusedBorderSettings = OutlineInputBorder(
        borderSide: BorderSide(width: 2, color: accentColor),
        borderRadius: BorderRadius.all(Radius.circular(radius)));

    final emailField = TextField(
        controller: emailController,
        obscureText: false,
        style: style,
        decoration: InputDecoration(
          fillColor: accentColor,
          prefixIcon: Icon(
            Icons.person_outline,
            color: accentColor,
          ),
          hintStyle: inputFieldTextStyle,
          labelStyle: inputFieldTextStyle,
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          labelText:
              TJLocalizations.of(context)?.getElement("RESET_TITLE") ?? "",
          focusedBorder: focusedBorderSettings,
          enabledBorder: focusedBorderSettings,
          border: borderSettings,
        ));

    final sendResetLink = Material(
      elevation: 5.0,
      color: accentColor,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width / 2,
        onPressed: () {
          resetFirebasePassword(emailController.text);
        },
        child: Text(
            TJLocalizations.of(context)?.getElement("RESET_SEND_RESET_LINK") ??
                "",
            textAlign: TextAlign.center,
            style: style.copyWith(color: Colors.white)),
      ),
    );

    return Scaffold(
        body: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                Colors.yellow[50]!,
                Colors.red[100]!,
              ],
            )),
            child: Stack(children: [
              Center(
                  child: SingleChildScrollView(
                      child: Padding(
                          padding: const EdgeInsets.all(36.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                TJLocalizations.of(context)
                                        ?.getElement("RESET_TITLE") ??
                                    "",
                                style: TextStyle(
                                    fontSize: 32, fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 16.0),
                              SizedBox(
                                height: 32.0,
                                child: Text(errorMessage,
                                    style: TextStyle(
                                        color:
                                            isError ? Colors.red : Colors.green,
                                        fontWeight: FontWeight.bold)),
                              ),
                              emailField,
                              SizedBox(height: 16.0),
                              sendResetLink,
                            ],
                          )))),
              new Positioned(
                  left: 24.0,
                  top: 32.0,
                  child: IconButton(
                    iconSize: 32,
                    color: accentColor,
                    onPressed: () => widget.callback(),
                    icon: Icon(Icons.arrow_back),
                  ))
            ])));
  }
}
