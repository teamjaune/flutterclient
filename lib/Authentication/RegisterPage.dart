import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:movie_suggestion_app/Config/Environment.dart';
import 'package:movie_suggestion_app/internationalization/ErrorLocalizations.dart';
import 'package:movie_suggestion_app/internationalization/TJLocalizations.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

String firebaseErrorTranslator(BuildContext context, String firebaseError) {
  if (firebaseError == 'invalid-email') {
    return ErrorLocalizations.of(context)!.getElement("invalid-email");
  } else if (firebaseError == 'email-already-in-use') {
    return ErrorLocalizations.of(context)!.getElement("email-already-in-use");
  } else if (firebaseError == 'operation-not-allowed') {
    //CALL Sentry
  } else if (firebaseError == 'weak-password') {
    return ErrorLocalizations.of(context)!.getElement("weak-password");
  }
  return ErrorLocalizations.of(context)!.getElement("operation-not-allowed");
}

class RegisterPage extends StatefulWidget {
  final Function callback;

  RegisterPage({required this.callback, Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<RegisterPage> {
  final emailController = TextEditingController();
  final passController = TextEditingController();
  final passConfirmController = TextEditingController();

  final double radius = 32;
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  String errorMessage = "";

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    emailController.dispose();
    passController.dispose();
    super.dispose();
  }

  void startFirebaseAuth(BuildContext context, String email, String password,
      String passwordConfirmation) async {
    if ("" == email || "" == password || "" == passwordConfirmation) {
      setState(() {
        errorMessage =
            ErrorLocalizations.of(context)!.getElement("all-fields-required");
      });
      return;
    }
    if (passwordConfirmation != password) {
      setState(() {
        errorMessage = ErrorLocalizations.of(context)!
            .getElement("password-confirmation-does-not-match");
      });
      return;
    }
    try {
      await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password);
    } on FirebaseAuthException catch (e) {
      print(e);
      setState(() {
        errorMessage = firebaseErrorTranslator(context, e.code);
      });
    } catch (exception, stackTrace) {
      if (Environment().config.reportErrors)
        await Sentry.captureException(
          exception,
          stackTrace: stackTrace,
        );
      else
        print(exception);
    }
  }

  @override
  Widget build(BuildContext context) {
    var accentColor = Colors.black;
    var inputFieldTextStyle = TextStyle(color: accentColor);
    var borderSettings = OutlineInputBorder(
        borderSide: BorderSide(width: 2, color: accentColor),
        borderRadius: BorderRadius.all(Radius.circular(radius)));
    var focusedBorderSettings = OutlineInputBorder(
        borderSide: BorderSide(width: 2, color: accentColor),
        borderRadius: BorderRadius.all(Radius.circular(radius)));

    final emailField = TextField(
        controller: emailController,
        obscureText: false,
        style: style,
        decoration: InputDecoration(
          fillColor: accentColor,
          prefixIcon: Icon(
            Icons.person_outline,
            color: accentColor,
          ),
          hintStyle: inputFieldTextStyle,
          labelStyle: inputFieldTextStyle,
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          labelText:
              TJLocalizations.of(context)?.getElement("REGISTER_EMAIL") ?? "",
          focusedBorder: focusedBorderSettings,
          enabledBorder: focusedBorderSettings,
          border: borderSettings,
        ));

    final passwordField = TextField(
        controller: passController,
        obscureText: true,
        style: style,
        decoration: InputDecoration(
            prefixIcon: Icon(Icons.lock_outline, color: accentColor),
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            labelText:
                TJLocalizations.of(context)?.getElement("REGISTER_PASSWORD") ??
                    "",
            labelStyle: inputFieldTextStyle,
            focusedBorder: focusedBorderSettings,
            enabledBorder: focusedBorderSettings,
            border: borderSettings));
    final passwordConfirmField = TextField(
        controller: passConfirmController,
        obscureText: true,
        style: style,
        decoration: InputDecoration(
            prefixIcon: Icon(Icons.lock_outline, color: accentColor),
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            labelText: TJLocalizations.of(context)
                    ?.getElement("REGISTER_PASSWORD_CONFIRM") ??
                "",
            labelStyle: inputFieldTextStyle,
            focusedBorder: focusedBorderSettings,
            enabledBorder: focusedBorderSettings,
            border: borderSettings));
    final registerButton = Material(
      elevation: 5.0,
      color: accentColor,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width / 2,
        onPressed: () {
          startFirebaseAuth(context, emailController.text, passController.text,
              passConfirmController.text);
        },
        child: Text(
            TJLocalizations.of(context)?.getElement("REGISTER_BUTTON") ?? "",
            textAlign: TextAlign.center,
            style: style.copyWith(color: Colors.white)),
      ),
    );

    return Scaffold(
        body: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                Colors.yellow[50]!,
                Colors.red[100]!,
              ],
            )),
            child: Stack(children: [
              Center(
                  child: SingleChildScrollView(
                      child: Padding(
                          padding: const EdgeInsets.all(36.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                TJLocalizations.of(context)
                                        ?.getElement("REGISTER_TITLE") ??
                                    "",
                                style: TextStyle(
                                    fontSize: 32, fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 16.0),
                              SizedBox(
                                height: 32.0,
                                child: Text(errorMessage,
                                    style: TextStyle(
                                        color: Colors.red,
                                        fontWeight: FontWeight.bold)),
                              ),
                              emailField,
                              SizedBox(height: 16.0),
                              passwordField,
                              SizedBox(height: 16.0),
                              passwordConfirmField,
                              SizedBox(height: 16.0),
                              registerButton,
                              SizedBox(height: 16.0),
                              InkWell(
                                  onTap: () {
                                    widget.callback();
                                  },
                                  child: Text(
                                    TJLocalizations.of(context)?.getElement(
                                            "REGISTER_ALREADY_HAVE_ACCOUNT") ??
                                        "",
                                    style: new TextStyle(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.bold),
                                  )),
                            ],
                          )))),
              new Positioned(
                  left: 24.0,
                  top: 32.0,
                  child: IconButton(
                    iconSize: 32,
                    color: accentColor,
                    onPressed: () => widget.callback(),
                    icon: Icon(Icons.arrow_back),
                  ))
            ])));
  }
}
