import 'dart:convert';

class TJHttpRespose {
  bool performed;
  int code;
  Map<String, dynamic> data;

  TJHttpRespose(
      {required bool performed,
      required int code,
      required Map<String, dynamic> data})
      : performed = performed,
        code = code,
        data = data;

  factory TJHttpRespose.fromString(String value) =>
      TJHttpRespose.fromMap(jsonDecode(value));

  factory TJHttpRespose.fromMap(Map<String, dynamic> json) => new TJHttpRespose(
      performed: json["performed"], code: json["code"], data: json["data"]);

  Map<String, dynamic> toMap() =>
      {"performed": performed, "code": code, "data": data};

  @override
  String toString() => """ performed : $performed code : $code""";
}
