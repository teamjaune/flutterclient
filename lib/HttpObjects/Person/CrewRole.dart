enum CrewRole {
  SELF,
  ACTOR,
  WRITER,
  EDITOR,
  ACTRESS,
  COMPOSER,
  DIRECTOR,
  PRODUCER,
  ARCHIVE_SOUND,
  ARCHIVE_FOOTAGE,
  CINEMATOGRAPHER,
  PRODUCTION_DESIGNER,
}

final Map<String, CrewRole> roleStringToEnum = {
  'self': CrewRole.SELF,
  'actor': CrewRole.ACTOR,
  'writer': CrewRole.WRITER,
  'editor': CrewRole.EDITOR,
  'actress': CrewRole.ACTRESS,
  'composer': CrewRole.COMPOSER,
  'producer': CrewRole.PRODUCER,
  'director': CrewRole.DIRECTOR,
  'archive_sound': CrewRole.ARCHIVE_SOUND,
  'cinematographer': CrewRole.CINEMATOGRAPHER,
  'archive_footage': CrewRole.ARCHIVE_FOOTAGE,
  'production_designer': CrewRole.PRODUCTION_DESIGNER
};

final Map<CrewRole, String> roleEnumToString = {
  CrewRole.SELF: 'self',
  CrewRole.ACTOR: 'actor',
  CrewRole.WRITER: 'writer',
  CrewRole.EDITOR: 'editor',
  CrewRole.ACTRESS: 'actress',
  CrewRole.COMPOSER: 'composer',
  CrewRole.PRODUCER: 'producer',
  CrewRole.DIRECTOR: 'director',
  CrewRole.ARCHIVE_SOUND: 'archive_sound',
  CrewRole.CINEMATOGRAPHER: 'cinematographer',
  CrewRole.ARCHIVE_FOOTAGE: 'archive_footage',
  CrewRole.PRODUCTION_DESIGNER: 'production_designer'
};
