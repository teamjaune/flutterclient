import 'PersonMoviesHttpResponse.dart';

class PersonInfoHttpResponse {
  late String personID;
  late String name;
  late int birthYear;
  late int? deathYear;
  late String biography;
  late String birthPlace;
  late String? imageURL;
  late PersonMoviesHttpResponse movies;
  PersonInfoHttpResponse(
      String personID,
      String name,
      int birthYear,
      int? deathYear,
      String biography,
      String birthPlace,
      String? imageURL,
      PersonMoviesHttpResponse movies) {
    this.personID = personID;
    this.birthYear = birthYear;
    this.deathYear = deathYear;
    this.name = name;
    this.imageURL = imageURL;
    this.movies = movies;
  }

  factory PersonInfoHttpResponse.fromMap(Map<String, dynamic> json) {
    PersonMoviesHttpResponse movies =
        PersonMoviesHttpResponse.fromList(json["movies"]);
    return new PersonInfoHttpResponse(
        json["person_id"],
        json["name"],
        json["birth_year"],
        json["death_year"],
        json["biography"],
        json["birth_place"],
        json["image_url"],
        movies);
  }

  Map<String, dynamic> toMap() => {
        "person_id": personID,
        "name": name,
        "birth_year": birthYear,
        "death_year": deathYear,
        "image_url": imageURL,
        "movies": movies
      };

  @override
  String toString() => """  
                            "name": $name
                            "birth_year": $birthYear
                            "death_year": $deathYear
                            "image_url": $imageURL
                            "movies" : $movies
                             """;
}
