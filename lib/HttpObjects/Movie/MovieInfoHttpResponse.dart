import 'MovieCrewInfoHttpResponse.dart';

enum MovieType {
  TV_SHORT,
  MOVIE,
  TV_MOVIE,
  SHORT,
  TV_MINI_SERIES,
  VIDEO,
  TV_SPECIAL,
  TV_SERIES
}

final Map<String, MovieType> typeStringToEnum = {
  "tvShort": MovieType.TV_SHORT,
  "movie": MovieType.MOVIE,
  "tvMovie": MovieType.TV_MOVIE,
  "short": MovieType.SHORT,
  "tvMiniSeries": MovieType.TV_MINI_SERIES,
  "video": MovieType.VIDEO,
  "tvSpecial": MovieType.TV_SPECIAL,
  "tvSeries": MovieType.TV_SERIES
};

final Map<MovieType, String> typeEnumToString = {
  MovieType.TV_SHORT: "Short",
  MovieType.MOVIE: "Movie",
  MovieType.TV_MOVIE: "Movie",
  MovieType.SHORT: "Short",
  MovieType.TV_MINI_SERIES: "Mini Series",
  MovieType.VIDEO: "Video",
  MovieType.TV_SPECIAL: "Special",
  MovieType.TV_SERIES: "TV Series "
};

class MovieInfoProductionCompanies {
  late String companyID;
  late String name;
  MovieInfoProductionCompanies(String companyID, String name) {
    this.companyID = companyID;
    this.name = name;
  }

  factory MovieInfoProductionCompanies.fromMap(Map<String, dynamic> json) {
    return new MovieInfoProductionCompanies(json["company_id"], json["name"]);
  }
}

class MovieInfoHttpResponse {
  late MovieType type;
  late String primaryTitle;
  late String originalTitle;
  late bool isAdult;
  late int startYear;
  int? endYear;
  late int runtimeMinutes;
  late double averageRating;
  late int numberVotes;
  late String imageURL;
  String? frenchTitle;
  late String synopsis;
  late double? ourAverageRating;
  double? rating;
  List<String>? genres;
  List<String>? keywords;
  List<MovieCrewInfoHttpResponse> crew = [];
  List<MovieInfoProductionCompanies> productionCompanies = [];

  MovieInfoHttpResponse(
      String type,
      String primaryTitle,
      String originalTitle,
      bool isAdult,
      int startYear,
      int? endYear,
      int runtimeMinutes,
      double averageRating,
      int numberVotes,
      String imageURL,
      String? frenchTitle,
      String synopsis,
      double? rating,
      double? ourAverageRating,
      List<dynamic>? genres,
      List<dynamic>? keywords,
      List<MovieCrewInfoHttpResponse> crew,
      List<MovieInfoProductionCompanies> productionCompanies) {
    this.primaryTitle = primaryTitle;
    this.originalTitle = originalTitle;
    this.isAdult = isAdult;
    this.startYear = startYear;
    this.endYear = endYear;
    this.runtimeMinutes = runtimeMinutes;
    this.averageRating = averageRating;
    this.numberVotes = numberVotes;
    this.imageURL = imageURL;
    this.frenchTitle = frenchTitle;
    this.synopsis = synopsis;
    this.rating = rating;
    this.ourAverageRating = ourAverageRating;
    this.type = typeStringToEnum[type] ?? MovieType.MOVIE;
    this.genres = genres!.cast<String>();
    this.keywords = keywords!.cast<String>();
    this.crew.addAll(crew);
    this.productionCompanies.addAll(productionCompanies);
  }

  factory MovieInfoHttpResponse.fromMap(Map<String, dynamic> json) {
    print(json['keywords']);
    List<MovieCrewInfoHttpResponse> people = List.from(json["crew"]
        .map<MovieCrewInfoHttpResponse>(
            (person) => MovieCrewInfoHttpResponse.fromMap(person)));
    List<MovieInfoProductionCompanies> companies = List.from(
        json["production_companies"].map<MovieInfoProductionCompanies>(
            (company) => MovieInfoProductionCompanies.fromMap(company)));
    return new MovieInfoHttpResponse(
        json["title_type"],
        json["primary_title"],
        json["original_title"],
        json["is_adult"],
        json["start_year"],
        json["end_year"],
        json["runtime_minutes"],
        json["average_rating"] * 1.0,
        json["number_votes"],
        json["image_url"],
        json["french_title"],
        json["synopsis"],
        (json["user_rating"] ?? 0) * 1.0,
        json["our_average_rating"],
        json["genres"],
        json["keywords"],
        people,
        companies);
  }

  Map<String, dynamic> toMap() => {
        "title_type": typeEnumToString[type],
        "primary_title": primaryTitle,
        "original_title": originalTitle,
        "is_adult": isAdult,
        "start_year": startYear,
        "end_year": endYear,
        "runtime_minutes": runtimeMinutes,
        "average_rating": averageRating,
        "number_votes": numberVotes,
        "image_url": imageURL,
        "french_title": frenchTitle,
        "synopsis": synopsis,
        "rating": rating,
        "our_average_rating": ourAverageRating,
        "keywords": keywords,
        "genres": genres
      };

  @override
  String toString() => """  title_type: ${typeEnumToString[type]}
                          primary_title: $primaryTitle
                          original_title: $originalTitle
                          is_adult: $isAdult
                          start_year: $startYear
                          end_year: $endYear
                          runtime_minutes: $runtimeMinutes
                          average_rating: $averageRating
                          number_votes: $numberVotes
                          image_url: $imageURL
                          french_title: $frenchTitle
                          synopsis: $synopsis
                          rating : $rating,
                          our_average_rating: $ourAverageRating,
                          keywords : $keywords,
                          genres : $genres """;
}
