import 'package:movie_suggestion_app/HttpObjects/Person/CrewRole.dart';

class MovieCrewInfoHttpResponse {
  late CrewRole role;
  late String personID;
  late String name;
  late int birthYear;
  late int? deathYear;
  late String? imageURL;

  MovieCrewInfoHttpResponse(
    String role,
    String personID,
    String name,
    int birthYear,
    int? deathYear,
    String? imageURL,
  ) {
    this.personID = personID;
    this.birthYear = birthYear;
    this.deathYear = deathYear;
    this.name = name;
    this.role = roleStringToEnum[role]!;
    this.imageURL = imageURL;
  }

  factory MovieCrewInfoHttpResponse.fromMap(Map<String, dynamic> json) {
    return new MovieCrewInfoHttpResponse(
        json["category"],
        json["person_id"],
        json["name"],
        json["birth_year"],
        json["death_year"],
        json["image_url"]);
  }

  Map<String, dynamic> toMap() => {
        "category": roleEnumToString[role],
        "name": name,
        "birth_year": birthYear,
        "death_year": deathYear,
        "image_url": imageURL
      };

  @override
  String toString() => """  "category": ${roleEnumToString[role]}
                            "name": $name
                            "birth_year": $birthYear
                            "death_year": $deathYear
                            "image_url": $imageURL """;
}
