import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' show SynchronousFuture;

class TJLocalizations {
  TJLocalizations(this.locale);

  final Locale locale;

  static TJLocalizations? of(BuildContext context) {
    return Localizations.of<TJLocalizations>(context, TJLocalizations);
  }

  static Map<String, Map<String, String>> _localizedValues = {
    'en': {
      //LOGIN PAGE
      'LOGIN_TITLE': 'Login',
      'LOGIN_EMAIL': 'Email',
      'LOGIN_PASSWORD': 'Password',
      'LOGIN_BUTTON': 'Login',
      'LOGIN_FORGOT_PASSWORD': 'Forgot Password ?',
      'LOGIN_CREATE_ACCOUNT': "Don't have and account ? Sign Up here",
      'LOGIN_DIVIDER_OR': 'OR',
      'LOGIN_WITH_GOOGLE': 'Sign In with Google',
      //REGISTER PAGE
      'REGISTER_TITLE': 'Register',
      'REGISTER_EMAIL': 'Email',
      'REGISTER_PASSWORD': 'Password',
      'REGISTER_PASSWORD_CONFIRM': 'Password Confirmation',
      'REGISTER_BUTTON': 'Register',
      'REGISTER_ALREADY_HAVE_ACCOUNT': 'Already have an account ? Sign In Here',
      //RESET PAGE
      'RESET_TITLE': 'Password Reset',
      'RESET_EMAIL': 'Email',
      'RESET_SEND_RESET_LINK': 'Send Reset Link',
      //SEARCH
      'SEARCH_HINT_TEXT': 'Search',
      'SEARCH_CATEGORY_MOVIE': 'Movies',
      'SEARCH_CATEGORY_CAST': 'Staff',
      'SEARCH_CATEGORY_COMPANIES': 'Companies',
      'SEARCH_CATEGORY_USERS': 'Users',
      'SEARCH_FILTER_YEAR_TITLE': 'Year Filter :',
      'SEARCH_FILTER_YEAR_TO': 'to',
      'SEARCH_NO_RESULT': 'No Results',
      'SEARCH_EMPTY_SEARCH': 'Write something in the search bar',
      'SEARCH_MOVIES_MOVIE_YEAR': "Year",
      'SEARCH_MOVIES_MOVIE_RATING': "Average Rating",
      'SEARCH_MOVIES_MOVIE_TYPE': "Type",
      'SEARCH_PEOPLE_BIRTH': "Birth Year",
      'SEARCH_PEOPLE_DEATH': "Death Year",
      //NAVIGATION
      'NAV_BAR_HOME': 'Home',
      'NAV_BAR_SEARH': 'Search',
      'NAV_BAR_SETTINGS': 'Settings',
      'NAV_BAR_PROFILE': 'Profile',
      //PROFILE
      'PROFILE_INFO_MOVIES': 'Movies',
      'PROFILE_INFO_FOLLOWING': 'Following',
      'PROFILE_INFO_FOLLOWERS': 'Followers',

      'PROFILE_INFO_EDIT_PROFILE_BUTTON': 'Edit Profile',
      'PROFILE_INFO_FOLLOW_BUTTON': 'Follow',
      'PROFILE_INFO_UNFOLLOW_BUTTON': 'Un Follow',

      'PROFILE_EDIT_DIALOG_TITLE': 'Edit Profile',
      'PROFILE_EDIT_DIALOG_USERNAME_FIELD': 'Username',
      'PROFILE_EDIT_DIALOG_SAVE_BUTTON': 'Save',
      'PROFILE_EDIT_DIALOG_CANCEL_BUTTON': 'Cancel',

      'PROFILE_TAB_TITLE_MOVIES': 'Rated Movies',
      'PROFILE_TAB_TITLE_FOLLOWING': 'Following',
      'PROFILE_TAB_TITLE_FOLLOWERS': 'Followers',

      'PROFILE_TAB_MOVIES_NO_RATED': 'You haven\'t rated any movies',

      'PROFILE_TAB_FOLLOWERS_NO_FOLLOWERS': 'You have no followers',
      'PROFILE_TAB_FOLLOWING_NO_FOLLOWING': 'You are not following anyone',
    },
    'fr': {
      //LOGIN PAGE
      'LOGIN_TITLE': 'Connexion',
      'LOGIN_EMAIL': 'Adresse mail',
      'LOGIN_PASSWORD': 'Mot de passe',
      'LOGIN_BUTTON': 'Se Connecter',
      'LOGIN_FORGOT_PASSWORD': 'Mot de passe oublié ?',
      'LOGIN_CREATE_ACCOUNT': "Pas encore inscrit? S'inscrire",
      'LOGIN_DIVIDER_OR': 'Ou',
      'LOGIN_WITH_GOOGLE': 'Continuer Avec Google',
      //REGISTER PAGE
      'REGISTER_TITLE': 'Inscription',
      'REGISTER_EMAIL': 'Adresse mail',
      'REGISTER_PASSWORD': 'Mot de passe',
      'REGISTER_PASSWORD_CONFIRM': 'Confirmer le mot de passe',
      'REGISTER_BUTTON': "S'inscrire",
      'REGISTER_ALREADY_HAVE_ACCOUNT': 'Déjà inscrit? Se connecter',
      //RESET PAGE
      'RESET_TITLE': 'Réinitialiser votre mot de passe',
      'RESET_EMAIL': 'Adresse mail',
      'RESET_SEND_RESET_LINK': 'Envoyer',
      //SEARCH
      'SEARCH_HINT_TEXT': 'Recherche',
      'SEARCH_CATEGORY_MOVIE': 'Films',
      'SEARCH_CATEGORY_CAST': 'Équipe',
      'SEARCH_CATEGORY_COMPANIES': '????????????',
      'SEARCH_CATEGORY_USERS': 'Utilisateurs',
      'SEARCH_FILTER_YEAR_TITLE': 'Filtrage par Année :',
      'SEARCH_FILTER_YEAR_TO': 'à',
      'SEARCH_NO_RESULT': 'Pas de resultats',
      'SEARCH_EMPTY_SEARCH': 'Write something in the search bar????',
      'SEARCH_MOVIES_MOVIE_YEAR': "Année",
      'SEARCH_MOVIES_MOVIE_RATING': "Note",
      'SEARCH_MOVIES_MOVIE_TYPE': "Type",
      'SEARCH_PEOPLE_BIRTH': "Année de naissance",
      'SEARCH_PEOPLE_DEATH': "Année de décès",
      //NAVIGATION
      'NAV_BAR_HOME': 'Accueil',
      'NAV_BAR_SEARH': 'Recherche',
      'NAV_BAR_SETTINGS': 'Settings',
      'NAV_BAR_PROFILE': 'Profil',
      //PROFILE
      'PROFILE_INFO_MOVIES': 'Movies',
      'PROFILE_INFO_FOLLOWING': 'Following',
      'PROFILE_INFO_FOLLOWERS': 'Followers',

      'PROFILE_INFO_EDIT_PROFILE_BUTTON': 'Edit Profile',
      'PROFILE_INFO_FOLLOW_BUTTON': 'Follow',
      'PROFILE_INFO_UNFOLLOW_BUTTON': 'Un Follow',

      'PROFILE_EDIT_DIALOG_TITLE': 'Edit Profile',
      'PROFILE_EDIT_DIALOG_USERNAME_FIELD': 'Username',
      'PROFILE_EDIT_DIALOG_SAVE_BUTTON': 'Save',
      'PROFILE_EDIT_DIALOG_CANCEL_BUTTON': 'Cancel',

      'PROFILE_TAB_TITLE_MOVIES': 'Rated Movies',
      'PROFILE_TAB_TITLE_FOLLOWING': 'Following',
      'PROFILE_TAB_TITLE_FOLLOWERS': 'Followers',

      'PROFILE_TAB_MOVIES_NO_RATED': 'You haven\'t rated any movies',

      'PROFILE_TAB_FOLLOWERS_NO_FOLLOWERS': 'You have no followers',
      'PROFILE_TAB_FOLLOWING_NO_FOLLOWING': 'You are not following anyone',
    },
  };

  String getElement(String id) {
    var lang = _localizedValues[locale.languageCode];
    return lang?[id] ?? "UI_TEXT_NOT_FOUND : " + id;
  }

  String get title {
    return _localizedValues[locale.languageCode]?['title'] ??
        "UI_TEXT_NOT_FOUND";
  }
}

class TJLocalizationsDelegate extends LocalizationsDelegate<TJLocalizations> {
  const TJLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'fr'].contains(locale.languageCode);

  @override
  Future<TJLocalizations> load(Locale locale) {
    // Returning a SynchronousFuture here because an async "load" operation
    // isn't needed to produce an instance of DemoLocalizations.
    return SynchronousFuture<TJLocalizations>(TJLocalizations(locale));
  }

  @override
  bool shouldReload(TJLocalizationsDelegate old) => false;
}
