import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' show SynchronousFuture;

class ErrorLocalizations {
  ErrorLocalizations(this.locale);

  final Locale locale;

  static ErrorLocalizations? of(BuildContext context) {
    return Localizations.of<ErrorLocalizations>(context, ErrorLocalizations);
  }

  static Map<String, Map<String, String>> _localizedValues = {
    'en': {
      //FIREBASE
      "invalid-email": "Provided email is badly formated",
      "user-disabled": "Your account is disabled, contact an administrator",
      "user-not-found": "Incorrect Email or Password",
      "wrong-password": "Incorrect Email or Password",
      "empty-email-or-password": "Email and Password fields cannot be empty",
      "email-already-in-use": "This email is already registered",
      "operation-not-allowed":
          "An issue occured please contact the administrator",
      "weak-password": "Your password is too weak",
      "all-fields-required": "All fields are required",
      "password-confirmation-does-not-match":
          "Password and Password Confirmation do not match",
    },
    'fr': {
      //FIREBASE
      "invalid-email": "L'email fourni n'est pas correct",
      "user-disabled":
          "Votre compte est désactivé, veuillez contacter un administrateur",
      "user-not-found": "Email ou Mot de Passe incorrect",
      "wrong-password": "Email ou Mot de Passe incorrect",
      "empty-email-or-password":
          "L'Email et le Mot de Passe ne peuvent pas être vides",
      "email-already-in-use": "Cet Email est déjà utilisé",
      "operation-not-allowed":
          "Une erreur est survenue, veuillez contacter un administrateur",
      "weak-password": "Le Mot de Passe fourni est trop faible",
      "all-fields-required": "Tous les champs sont requis",
      "password-confirmation-does-not-match":
          "Les Mots de Passe ne correspondent pas",
    },
  };

  String getElement(String id) {
    var lang = _localizedValues[locale.languageCode];
    return lang?[id] ?? "UI_TEXT_NOT_FOUND";
  }
}

class DaleelErrorLocalizationsDelegate
    extends LocalizationsDelegate<ErrorLocalizations> {
  const DaleelErrorLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'fr'].contains(locale.languageCode);

  @override
  Future<ErrorLocalizations> load(Locale locale) {
    // Returning a SynchronousFuture here because an async "load" operation
    // isn't needed to produce an instance of DemoLocalizations.
    return SynchronousFuture<ErrorLocalizations>(ErrorLocalizations(locale));
  }

  @override
  bool shouldReload(DaleelErrorLocalizationsDelegate old) => false;
}
