import 'package:flutter/material.dart';

Color generateFromHexString(String hex) {
  if (hex.length == 6) {
    var r = int.parse(hex.substring(0, 2), radix: 16);
    var g = int.parse(hex.substring(2, 4), radix: 16);
    var b = int.parse(hex.substring(4, 6), radix: 16);
    return Color.fromRGBO(r, g, b, 1);
  } else if (hex.length == 8) {
    var a = int.parse(hex.substring(0, 2), radix: 16);
    var r = int.parse(hex.substring(2, 4), radix: 16);
    var g = int.parse(hex.substring(4, 6), radix: 16);
    var b = int.parse(hex.substring(6, 8), radix: 16);
    return Color.fromARGB(a, r, g, b);
  }
  return Colors.white;
}

class Themes {
  static ThemeData get lightTheme {
    //1
    return ThemeData(
      // Define the default brightness and colors.
      brightness: Brightness.light,
      primaryColor: Colors.red,
      colorScheme: ColorScheme(
          primary: generateFromHexString("000000"),
          primaryVariant: generateFromHexString("cccccc"),
          secondary: generateFromHexString("f5aba2"),
          secondaryVariant: generateFromHexString("8e4e48"),
          surface: generateFromHexString("E5E5E5"),
          background: generateFromHexString("f2f2f2"),
          error: generateFromHexString("ff0000"),
          onPrimary: generateFromHexString("ffffff"),
          onSecondary: generateFromHexString("000000"),
          onSurface: generateFromHexString("000000"),
          onBackground: generateFromHexString("000000"),
          onError: generateFromHexString("000000"),
          brightness: Brightness.light),
      // Define the default font family.
      fontFamily: 'Lato',

      // Define the default `TextTheme`. Use this to specify the default
      // text styling for headlines, titles, bodies of text, and more.
      textTheme: const TextTheme(
        headline1: TextStyle(fontSize: 96.0, fontWeight: FontWeight.bold),
        headline6: TextStyle(fontSize: 20.0),
        bodyText2: TextStyle(fontSize: 15.0),
      ),
    );
  }
}
