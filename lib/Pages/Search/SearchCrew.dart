import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:movie_suggestion_app/internationalization/TJLocalizations.dart';
import 'package:movie_suggestion_app/utilities/Communications.dart';
import 'package:movie_suggestion_app/Config/Globals.dart';
import 'package:movie_suggestion_app/utilities/Debouncer.dart';
import 'package:movie_suggestion_app/widgets/Person/PersonDisplayDetached.dart';
import 'Http/SearchCrewHttpResponse.dart';

class Repository {
  static Future<SearchCrewHttpResponse> fetch(String searchString, int offset) {
    return sendPostRequest(SEARCH_ENDPOINT_PEOPLE, [],
        {"searchText": searchString, "offset": offset}).then((result) async {
      switch (result!.statusCode) {
        case 200:
          return SearchCrewHttpResponse.fromList(
              await getJsonStringFromResponse(result));
      }
      return Future.value(SearchCrewHttpResponse.fromList([]));
    }).catchError((err) {
      //Handle response error
      return Future.value(SearchCrewHttpResponse.fromList([]));
    });
  }
}

class SearchCrew extends StatefulWidget {
  final ValueNotifier<String> searchText;

  SearchCrew({Key? key, required this.searchText}) : super(key: key);

  @override
  _SearchCrewState createState() => _SearchCrewState();
}

class _SearchCrewState extends State<SearchCrew> {
  bool _isLoading = false;
  List<SearchCrewHttpResponseItem> _crew = [];
  final Debouncer _debounce = Debouncer(Duration(milliseconds: 400));

  void _fetchData(int offset) async {
    setState(() {
      _isLoading = true;
    });
    // Ignore Adding results of old requests, trust me this shouldn't be removed
    var originalSearchText = this.widget.searchText.value;
    var res = await Repository.fetch(this.widget.searchText.value, offset);
    if (this.widget.searchText.value == originalSearchText)
      setState(() {
        _crew.addAll(res.items);
        _isLoading = false;
      });
  }

  void onSearchTextUpdate() {
    _debounce(() {
      setState(() {
        _crew.clear();
        _fetchData(_crew.length);
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _fetchData(_crew.length);
    this.widget.searchText.addListener(onSearchTextUpdate);
  }

  @override
  void dispose() {
    super.dispose();
    this.widget.searchText.removeListener(onSearchTextUpdate);
    _crew.clear();
  }

  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  Widget buildPeopleList() {
    List<SearchCrewHttpResponseItem> people = _crew;
    if (people.length == 0 && !_isLoading)
      return Center(
          child: Padding(
              padding: EdgeInsets.fromLTRB(0, 8, 0, 0),
              child: Text(TJLocalizations.of(context)!
                  .getElement("SEARCH_NO_RESULT"))));
    return ListView.builder(
      itemCount: people.length,
      itemBuilder: (context, index) {
        var element = people[index];
        return GestureDetector(
          child: Card(
              shadowColor: Colors.red,
              child: SizedBox(
                  height: 128,
                  child: Container(
                    padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                              flex: 2,
                              child: Container(
                                padding: EdgeInsets.zero,
                                child: FadeInImage(
                                  placeholder:
                                      AssetImage("assets/images/noposter.png"),
                                  image: element.imageURL == null
                                      ? AssetImage("assets/images/noposter.png")
                                          as ImageProvider
                                      : NetworkImage(element.imageURL!),
                                  fit: BoxFit.fitHeight,
                                ),
                                margin: EdgeInsets.zero,
                              )),
                          Expanded(
                              flex: 8,
                              child: Container(
                                  margin: EdgeInsets.all(16),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      RichText(
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        text: TextSpan(
                                            style:
                                                TextStyle(color: Colors.black),
                                            text: element.name),
                                        textScaleFactor: 1.2,
                                      ),
                                      Text(
                                          "${element.birthYear} ${element.deathYear == null ? "" : "- ${element.deathYear}"}")
                                    ],
                                  )))
                        ]),
                  ))),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      PersonDetachedWidget(personID: element.personID)),
            );
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return (this.widget.searchText.value.isEmpty
        ? Center(
            child: Padding(
                padding: EdgeInsets.fromLTRB(0, 8, 0, 0),
                child: Text(TJLocalizations.of(context)!
                    .getElement("SEARCH_EMPTY_SEARCH"))))
        : Column(children: [
            Expanded(
                child: NotificationListener<ScrollNotification>(
                    onNotification: (ScrollNotification scrollInfo) {
                      if (scrollInfo is UserScrollNotification) {
                        if (scrollInfo.direction == ScrollDirection.reverse) {
                          if (!_isLoading &&
                              scrollInfo.metrics.pixels ==
                                  scrollInfo.metrics.maxScrollExtent) {
                            _fetchData(_crew.length);
                            // start loading data
                            setState(() {
                              _isLoading = true;
                            });
                          }
                        }
                      }
                      return true;
                    },
                    child: buildPeopleList())),
            Container(
              height: _isLoading ? 50.0 : 0,
              color: Colors.transparent,
              child: Center(
                child: new CircularProgressIndicator(),
              ),
            ),
          ]));
  }
}
