class SearchCompanyResponseItem {
  late String companyID;
  late String name;
  late String? description;
  late String? imageURL;
  late String? originalCountry;

  SearchCompanyResponseItem(
    String companyID,
    String name,
    String? description,
    String? imageURL,
    String? originalCountry,
  ) {
    this.companyID = companyID;
    this.name = name;
    this.description = description;
    this.imageURL = imageURL;
    this.originalCountry = originalCountry;
  }

  factory SearchCompanyResponseItem.fromMap(Map<String, dynamic> json) {
    return new SearchCompanyResponseItem(json["company_id"], json["name"],
        json["description"], json["image_url"], json["origin_country"]);
  }

  get movieID => null;

  Map<String, dynamic> toMap() => {
        "company_id": companyID,
        "name": name,
        "description": description,
        "image_url": imageURL,
        "origin_country": originalCountry
      };

  @override
  String toString() => """
        "company_id": $companyID,
        "name": $name,
        "description": $description,
        "image_url": $imageURL,
        "origin_country": $originalCountry
        """;
}

class SearchCompanyResponse {
  List<SearchCompanyResponseItem> items;

  SearchCompanyResponse({required this.items});

  factory SearchCompanyResponse.fromList(List companyList) {
    List<SearchCompanyResponseItem> items = [];
    companyList.forEach((company) {
      items.add(SearchCompanyResponseItem.fromMap(company));
    });
    return new SearchCompanyResponse(items: items);
  }

  @override
  String toString() => """ List : $items""";
}
