import 'package:movie_suggestion_app/HttpObjects/Movie/MovieInfoHttpResponse.dart';

class SearchMoviesHttpResponseItem {
  late String movieID;
  late MovieType type;
  late String primaryTitle;
  late String originalTitle;
  late bool isAdult;
  late int startYear;
  int? endYear;
  late int runtimeMinutes;
  late double averageRating;
  late int numberVotes;
  late String imageURL;
  String? frenchTitle;

  SearchMoviesHttpResponseItem(
      String movieID,
      String type,
      String primaryTitle,
      String originalTitle,
      bool isAdult,
      int startYear,
      int? endYear,
      int runtimeMinutes,
      double averageRating,
      int numberVotes,
      String imageURL,
      String? frenchTitle) {
    this.movieID = movieID;
    this.primaryTitle = primaryTitle;
    this.originalTitle = originalTitle;
    this.isAdult = isAdult;
    this.startYear = startYear;
    this.endYear = endYear;
    this.runtimeMinutes = runtimeMinutes;
    this.averageRating = averageRating;
    this.numberVotes = numberVotes;
    this.imageURL = imageURL;
    this.frenchTitle = frenchTitle;
    this.type = typeStringToEnum[type] ?? MovieType.MOVIE;
  }

  factory SearchMoviesHttpResponseItem.fromMap(Map<String, dynamic> json) {
    return new SearchMoviesHttpResponseItem(
        json["movie_id"],
        json["title_type"],
        json["primary_title"],
        json["original_title"],
        json["is_adult"],
        json["start_year"],
        json["end_year"],
        json["runtime_minutes"],
        json["average_rating"] * 1.0,
        json["number_votes"],
        json["image_url"],
        json["french_title"]);
  }

  Map<String, dynamic> toMap() => {
        "movie_id": movieID,
        "title_type": typeEnumToString[type],
        "primary_title": primaryTitle,
        "original_title": originalTitle,
        "is_adult": isAdult,
        "start_year": startYear,
        "end_year": endYear,
        "runtime_minutes": runtimeMinutes,
        "average_rating": averageRating,
        "number_votes": numberVotes,
        "image_url": imageURL,
        "french_title": frenchTitle
      };

  @override
  String toString() => """  title_type: ${typeEnumToString[type]}
                          primary_title: $primaryTitle
                          original_title: $originalTitle
                          is_adult: $isAdult
                          start_year: $startYear
                          end_year: $endYear
                          runtime_minutes: $runtimeMinutes
                          average_rating: $averageRating
                          number_votes: $numberVotes
                          image_url: $imageURL
                          french_title: $frenchTitle""";
}

class SearchMoviesHttpResponse {
  List<SearchMoviesHttpResponseItem> items;

  SearchMoviesHttpResponse({required this.items});

  factory SearchMoviesHttpResponse.fromList(List movies) {
    List<SearchMoviesHttpResponseItem> items = [];
    movies.forEach((movie) {
      items.add(SearchMoviesHttpResponseItem.fromMap(movie));
    });
    return new SearchMoviesHttpResponse(items: items);
  }

  @override
  String toString() => """ List : $items""";
}
