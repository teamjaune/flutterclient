class SearchUserHttpResponseItem {
  late int userID;
  late String username;
  late String? imageURL;
  late bool isFollowed;

  SearchUserHttpResponseItem(
      int userID, String username, String? imageURL, bool isFollowed) {
    this.userID = userID;
    this.username = username;
    this.imageURL = imageURL;
    this.isFollowed = isFollowed;
  }

  factory SearchUserHttpResponseItem.fromMap(Map<String, dynamic> json) {
    return new SearchUserHttpResponseItem(
        json["id"], json["username"], json["picture"], json["is_followed"]);
  }

  get movieID => null;

  Map<String, dynamic> toMap() => {
        "id": userID,
        "username": username,
        "picture": imageURL,
        "is_followed": isFollowed
      };

  @override
  String toString() => """
        "company_id": $userID,
        "name": $username,
        "description": $imageURL,
        "image_url": $imageURL,
        "origin_country": $isFollowed
        """;
}

class SearchUserHttpResponse {
  List<SearchUserHttpResponseItem> items;

  SearchUserHttpResponse({required this.items});

  factory SearchUserHttpResponse.fromList(List usersList) {
    List<SearchUserHttpResponseItem> items = [];
    usersList.forEach((user) {
      items.add(SearchUserHttpResponseItem.fromMap(user));
    });
    return new SearchUserHttpResponse(items: items);
  }

  @override
  String toString() => """ List : $items""";
}
