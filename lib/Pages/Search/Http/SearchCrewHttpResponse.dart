class SearchCrewHttpResponseItem {
  late String personID;
  late String name;
  late int birthYear;
  late int? deathYear;
  late String? imageURL;
  late String? birthPlace;
  late String? biography;
  late int? numberRoles;

  SearchCrewHttpResponseItem(
    String personID,
    String name,
    int birthYear,
    int? deathYear,
    String? imageURL,
    String? birthPlace,
    String? biography,
    int? numberRoles,
  ) {
    this.personID = personID;
    this.name = name;
    this.birthYear = birthYear;
    this.deathYear = deathYear;
    this.imageURL = imageURL;
    this.birthPlace = birthPlace;
    this.biography = biography;
    this.numberRoles = numberRoles;
  }

  factory SearchCrewHttpResponseItem.fromMap(Map<String, dynamic> json) {
    return new SearchCrewHttpResponseItem(
        json["person_id"],
        json["name"],
        json["birth_year"],
        json["death_year"],
        json["image_url"],
        json["biography"],
        json["birth_place"],
        json["number_roles"]);
  }

  get movieID => null;

  Map<String, dynamic> toMap() => {
        "person_id": personID,
        "name": name,
        "birth_year": birthYear,
        "death_year": deathYear,
        "image_url": imageURL,
        "number_roles": numberRoles
      };

  @override
  String toString() => """
        "person_id": $personID,
        "name": $name,
        "birth_year":$birthYear,
        "death_year": $deathYear,
        "image_url": $imageURL,
        "number_roles": $numberRoles""";
}

class SearchCrewHttpResponse {
  List<SearchCrewHttpResponseItem> items;

  SearchCrewHttpResponse({required this.items});

  factory SearchCrewHttpResponse.fromList(List people) {
    List<SearchCrewHttpResponseItem> items = [];
    people.forEach((person) {
      items.add(SearchCrewHttpResponseItem.fromMap(person));
    });
    return new SearchCrewHttpResponse(items: items);
  }

  @override
  String toString() => """ List : $items""";
}
