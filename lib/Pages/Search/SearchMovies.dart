import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:movie_suggestion_app/internationalization/TJLocalizations.dart';
import 'package:movie_suggestion_app/utilities/Communications.dart';
import 'package:movie_suggestion_app/utilities/Debouncer.dart';
import 'package:movie_suggestion_app/Config/Globals.dart';
import 'package:movie_suggestion_app/widgets/Movie/MovieDisplayDetached.dart';
import 'package:movie_suggestion_app/widgets/Search/GenreFilter.dart';
import 'package:movie_suggestion_app/widgets/Search/YearFilter.dart';
import '../../HttpObjects/Movie/MovieInfoHttpResponse.dart';
import 'Http/SearchMoviesHttpResponse.dart';

class Repository {
  static Future<SearchMoviesHttpResponse> fetch(
      String searchString,
      List<String> ignoredGenres,
      bool isAdult,
      int startYear,
      int endYear,
      int offset) {
    return sendPostRequest(SEARCH_ENDPOINT_MOVIE, [], {
      "searchText": searchString,
      "genresToIgnore": ignoredGenres,
      "familyFriendly": isAdult,
      "startYear": startYear,
      "endYear": endYear,
      "offset": offset
    }).then((result) async {
      switch (result!.statusCode) {
        case 200:
          return SearchMoviesHttpResponse.fromList(
              await getJsonStringFromResponse(result));
      }
      return Future.value(SearchMoviesHttpResponse.fromList([]));
    }).catchError((err) {
      //Handle response error
      return Future.value(SearchMoviesHttpResponse.fromList([]));
    });
  }
}

class SearchMovies extends StatefulWidget {
  final ValueNotifier<String> searchText;

  SearchMovies({Key? key, required this.searchText}) : super(key: key);

  @override
  _SearchMoviesState createState() => _SearchMoviesState();
}

class _SearchMoviesState extends State<SearchMovies> {
  int _from = 0;
  int _to = 99999;
  List<String> _ignoredGenres = [];
  bool _isLoading = false;
  List<SearchMoviesHttpResponseItem> _movies = [];
  final Debouncer _debounce = Debouncer(Duration(milliseconds: 400));

  void _fetchData(int offset) async {
    setState(() {
      _isLoading = true;
    });
    // Ignore Adding results of old requests, trust me this shouldn't be removed
    var originalSearchText = this.widget.searchText.value;
    var res = await Repository.fetch(
        this.widget.searchText.value, _ignoredGenres, true, _from, _to, offset);
    if (this.widget.searchText.value == originalSearchText)
      setState(() {
        _movies.addAll(res.items);
        _isLoading = false;
      });
  }

  void onSearchTextUpdate() {
    _debounce(() {
      setState(() {
        _movies.clear();
        _fetchData(_movies.length);
      });
    });
  }

  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    super.initState();
    _fetchData(_movies.length);
    this.widget.searchText.addListener(onSearchTextUpdate);
  }

  @override
  void dispose() {
    super.dispose();
    this.widget.searchText.removeListener(onSearchTextUpdate);
    _movies.clear();
  }

  void setYear(int from, int to) {
    _debounce(() {
      setState(() {
        _from = from;
        _to = to;
        _movies.clear();
        _fetchData(_movies.length);
      });
    });
  }

  void setGenreToIgnore(List<String> genres) {
    _debounce(() {
      setState(() {
        _ignoredGenres.clear();
        _ignoredGenres.addAll(genres);
        _movies.clear();
        _fetchData(_movies.length);
      });
    });
  }

  Widget buildMoviesList() {
    List<SearchMoviesHttpResponseItem> movies = _movies;
    if (movies.length == 0 && !_isLoading)
      return Center(
          child: Padding(
              padding: EdgeInsets.fromLTRB(0, 8, 0, 0),
              child: Text(TJLocalizations.of(context)!
                  .getElement("SEARCH_NO_RESULT"))));
    return ListView.builder(
      itemCount: movies.length,
      itemBuilder: (context, index) {
        var element = movies[index];
        return GestureDetector(
          child: Card(
              shadowColor: Colors.red,
              child: SizedBox(
                  height: 128,
                  child: Container(
                    padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                              flex: 2,
                              child: Container(
                                padding: EdgeInsets.zero,
                                child: FadeInImage(
                                  placeholder:
                                      AssetImage("assets/images/noposter.png"),
                                  image: element.imageURL == null
                                      ? AssetImage("assets/images/noposter.png")
                                          as ImageProvider
                                      : NetworkImage(element.imageURL),
                                  fit: BoxFit.fitHeight,
                                ),
                                margin: EdgeInsets.zero,
                              )),
                          Expanded(
                              flex: 8,
                              child: Container(
                                  margin: EdgeInsets.all(16),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      RichText(
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        text: TextSpan(
                                            style:
                                                TextStyle(color: Colors.black),
                                            text: element.originalTitle),
                                        textScaleFactor: 1.2,
                                      ),
                                      Text(
                                          "${TJLocalizations.of(context)!.getElement("SEARCH_MOVIES_MOVIE_YEAR")} : ${element.startYear}"),
                                      Text(
                                          "${TJLocalizations.of(context)!.getElement("SEARCH_MOVIES_MOVIE_RATING")} : ${element.averageRating}"),
                                      Text(
                                          "${TJLocalizations.of(context)!.getElement("SEARCH_MOVIES_MOVIE_TYPE")} : ${typeEnumToString[element.type]}")
                                    ],
                                  )))
                        ]),
                  ))),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      MovieDetachedWidget(movieID: element.movieID)),
            );
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return (Column(children: [
      GenreFilter(callBack: setGenreToIgnore),
      YearFilter(callBack: setYear),
      Divider(height: 1),
      this.widget.searchText.value.isEmpty
          ? Expanded(
              child: Center(
                  child: Padding(
                      padding: EdgeInsets.fromLTRB(0, 8, 0, 0),
                      child: Text(TJLocalizations.of(context)!
                          .getElement("SEARCH_EMPTY_SEARCH")))))
          : Expanded(
              child: Column(children: [
              if (_movies.length > 0)
                Expanded(
                    child: NotificationListener<ScrollNotification>(
                        onNotification: (ScrollNotification scrollInfo) {
                          if (scrollInfo is UserScrollNotification) {
                            if (scrollInfo.direction ==
                                ScrollDirection.reverse) {
                              if (!_isLoading &&
                                  scrollInfo.metrics.pixels ==
                                      scrollInfo.metrics.maxScrollExtent) {
                                _fetchData(_movies.length);
                                // start loading data
                                setState(() {
                                  _isLoading = true;
                                });
                              }
                            }
                          }
                          return true;
                        },
                        child: buildMoviesList())),
              Center(
                child: Container(
                  height: _isLoading ? 50.0 : 0,
                  color: Colors.transparent,
                  child: Center(
                    child: new CircularProgressIndicator(),
                  ),
                ),
              ),
            ]))
    ]));
  }
}
