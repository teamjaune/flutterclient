import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:movie_suggestion_app/Pages/Profile/ProfileDetachedWidget.dart';
import 'package:movie_suggestion_app/internationalization/TJLocalizations.dart';
import 'package:movie_suggestion_app/utilities/AppState.dart';
import 'package:movie_suggestion_app/utilities/Communications.dart';
import 'package:movie_suggestion_app/Config/Globals.dart';
import 'package:movie_suggestion_app/utilities/Debouncer.dart';
import 'package:provider/src/provider.dart';
import 'Http/SearchUserHttpResponse.dart';

class Repository {
  static Future<SearchUserHttpResponse> fetch(String searchString, int offset) {
    return sendPostRequest(SEARCH_ENDPOINT_USERS, [],
        {"searchText": searchString, "offset": offset}).then((result) async {
      switch (result!.statusCode) {
        case 200:
          return SearchUserHttpResponse.fromList(
              await getJsonStringFromResponse(result));
      }
      return Future.value(SearchUserHttpResponse.fromList([]));
    }).catchError((err) {
      //Handle response error
      return Future.value(SearchUserHttpResponse.fromList([]));
    });
  }
}

class SearchUsers extends StatefulWidget {
  final ValueNotifier<String> searchText;

  SearchUsers({Key? key, required this.searchText}) : super(key: key);

  @override
  _SearchUsersState createState() => _SearchUsersState();
}

class _SearchUsersState extends State<SearchUsers> {
  bool _isLoading = false;
  List<SearchUserHttpResponseItem> _users = [];
  final Debouncer _debounce = Debouncer(Duration(milliseconds: 400));

  void _fetchData(int offset) async {
    setState(() {
      _isLoading = true;
    });
    // Ignore Adding results of old requests, trust me this shouldn't be removed
    var originalSearchText = this.widget.searchText.value;
    var res = await Repository.fetch(this.widget.searchText.value, offset);
    if (this.widget.searchText.value == originalSearchText)
      setState(() {
        _users.addAll(res.items);
        _isLoading = false;
      });
  }

  void onSearchTextUpdate() {
    _debounce(() {
      setState(() {
        _users.clear();
        _fetchData(_users.length);
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _fetchData(_users.length);
    this.widget.searchText.addListener(onSearchTextUpdate);
  }

  @override
  void dispose() {
    super.dispose();
    this.widget.searchText.removeListener(onSearchTextUpdate);
    _users.clear();
  }

  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  Widget buildList() {
    List<SearchUserHttpResponseItem> users = _users;
    if (users.length == 0 && !_isLoading)
      return Center(
          child: Padding(
              padding: EdgeInsets.fromLTRB(0, 8, 0, 0),
              child: Text(TJLocalizations.of(context)!
                  .getElement("SEARCH_NO_RESULT"))));
    return ListView.builder(
      itemCount: users.length,
      itemBuilder: (context, index) {
        var element = users[index];
        return GestureDetector(
          child: Card(
              shadowColor: Colors.red,
              child: SizedBox(
                  height: 94,
                  child: Container(
                    padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                              flex: 2,
                              child: Container(
                                padding: EdgeInsets.zero,
                                child: CircleAvatar(
                                  backgroundImage: NetworkImage(
                                      context.read<AppState>().serverAddress +
                                          PROFILE_IMAGE_URL +
                                          (element.userID).toString()),
                                  radius: 120.0,
                                ),
                                margin: EdgeInsets.zero,
                              )),
                          Expanded(
                              flex: 8,
                              child: Container(
                                  margin: EdgeInsets.all(16),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      RichText(
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        text: TextSpan(
                                            style:
                                                TextStyle(color: Colors.black),
                                            text: element.username),
                                        textScaleFactor: 1.2,
                                      )
                                    ],
                                  )))
                        ]),
                  ))),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      ProfileDetachedWidget(userID: element.userID)),
            );
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return (this.widget.searchText.value.isEmpty
        ? Center(
            child: Padding(
                padding: EdgeInsets.fromLTRB(0, 8, 0, 0),
                child: Text(TJLocalizations.of(context)!
                    .getElement("SEARCH_EMPTY_SEARCH"))))
        : Column(children: [
            Expanded(
                child: NotificationListener<ScrollNotification>(
                    onNotification: (ScrollNotification scrollInfo) {
                      if (scrollInfo is UserScrollNotification) {
                        if (scrollInfo.direction == ScrollDirection.reverse) {
                          if (!_isLoading &&
                              scrollInfo.metrics.pixels ==
                                  scrollInfo.metrics.maxScrollExtent) {
                            _fetchData(_users.length);
                            // start loading data
                            setState(() {
                              _isLoading = true;
                            });
                          }
                        }
                      }
                      return true;
                    },
                    child: buildList())),
            Container(
              height: _isLoading ? 50.0 : 0,
              color: Colors.transparent,
              child: Center(
                child: new CircularProgressIndicator(),
              ),
            ),
          ]));
  }
}
