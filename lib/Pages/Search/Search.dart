import 'package:flutter/material.dart';
import 'package:movie_suggestion_app/Pages/Search/SearchCompanies.dart';
import 'package:movie_suggestion_app/Pages/Search/SearchCrew.dart';
import 'package:movie_suggestion_app/Pages/Search/SearchMovies.dart';
import 'package:movie_suggestion_app/internationalization/TJLocalizations.dart';

import 'SearchUsers.dart';

class Search extends StatefulWidget {
  Search({Key? key}) : super(key: key);

  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  TextEditingController _searchController = new TextEditingController();
  bool searching = false;
  ValueNotifier<String> searchText = ValueNotifier("");

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        backgroundColor: Theme.of(context).colorScheme.background,
        body: Column(children: [
          TextField(
            onChanged: (value) {
              searchText.value = value;
            },
            textAlignVertical: TextAlignVertical.center,
            controller: _searchController,
            decoration: InputDecoration(
              prefixIcon: Icon(Icons.search),
              filled: true,
              hintStyle: TextStyle(color: Colors.grey),
              hintText:
                  TJLocalizations.of(context)!.getElement("SEARCH_HINT_TEXT"),
              fillColor: Colors.white,
              suffixIcon: IconButton(
                onPressed: () {
                  searchText.value = "";
                  _searchController.clear();
                },
                icon: Icon(Icons.clear),
              ),
            ),
          ),
          Expanded(
              child: DefaultTabController(
                  length: 4,
                  child: Scaffold(
                      backgroundColor: Theme.of(context).colorScheme.background,
                      appBar: TabBar(
                        indicatorColor: Theme.of(context).colorScheme.primary,
                        labelColor: Theme.of(context).colorScheme.onSecondary,
                        tabs: [
                          Tab(
                              text: TJLocalizations.of(context)!
                                  .getElement("SEARCH_CATEGORY_MOVIE")),
                          Tab(
                              text: TJLocalizations.of(context)!
                                  .getElement("SEARCH_CATEGORY_CAST")),
                          Tab(
                              text: TJLocalizations.of(context)!
                                  .getElement("SEARCH_CATEGORY_COMPANIES")),
                          Tab(
                              text: TJLocalizations.of(context)!
                                  .getElement("SEARCH_CATEGORY_USERS")),
                        ],
                      ),
                      body: TabBarView(
                        children: [
                          SearchMovies(searchText: searchText),
                          SearchCrew(searchText: searchText),
                          SearchCompanies(searchText: searchText),
                          SearchUsers(searchText: searchText),
                        ],
                      ))))
        ]));
  }
}
