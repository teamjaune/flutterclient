import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:movie_suggestion_app/Config/Globals.dart';
import 'package:movie_suggestion_app/Pages/Search/Search.dart';
import 'package:movie_suggestion_app/Pages/Settings.dart';
import 'package:movie_suggestion_app/internationalization/TJLocalizations.dart';
import 'package:movie_suggestion_app/utilities/Communications.dart';
import 'package:movie_suggestion_app/widgets/Movie/MovieDisplay.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'Home.dart';
import 'Profile/Profile.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';

String buildFollowRequestUrl(userID) => "$PROFILE_ENDPOINT/$userID/follow";

class PageHandler extends StatefulWidget {
  PageHandler({Key? key}) : super(key: key);

  @override
  _PageHandlerState createState() => _PageHandlerState();
}

class _PageHandlerState extends State<PageHandler> {
  int _selectedIndex = 0;

  static List<Widget> _widgetOptions = <Widget>[
    Home(),
    Search(),
    Profile(),
    Settings()
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  Future<bool> sendFollowRequest(int userID) {
    return sendPutRequest(buildFollowRequestUrl(userID), [], {}).then((result) {
      switch (result!.statusCode) {
        case 200:
          return true;
      }
      return false;
    }).catchError((err) {
      //Handle response error
      return false;
    });
  }

  void barcodeWidget() {
    showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text("Your Friend Code"),
            IconButton(
                onPressed: () async {
                  String barcode = await FlutterBarcodeScanner.scanBarcode(
                      "#ff6666", "Cancel", false, ScanMode.QR);
                  try {
                    var decodedBarcode = jsonDecode(barcode);
                    if (decodedBarcode["operation"] == null) return;
                    switch (decodedBarcode["operation"]) {
                      case "follow":
                        if (decodedBarcode["user_id"] != null) {
                          sendFollowRequest(decodedBarcode["user_id"]);
                          ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(content: Text("Friend Added")));
                        }
                        break;
                    }
                  } catch (e) {}
                },
                icon: Icon(Icons.camera_alt))
          ]),
          content: QrImage(
            data: "{\"operation\":\"follow\", \"user_id\":1}",
            version: QrVersions.auto,
            gapless: true,
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            onPressed: () async {
              barcodeWidget();
            },
            icon: Icon(Icons.qr_code)),
        centerTitle: true,
        title: Text("Movie Suggester"),
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: TJLocalizations.of(context)!.getElement("NAV_BAR_HOME"),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: TJLocalizations.of(context)!.getElement("NAV_BAR_SEARH"),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: TJLocalizations.of(context)!.getElement("NAV_BAR_PROFILE"),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: TJLocalizations.of(context)!.getElement("NAV_BAR_SETTINGS"),
          ),
        ],
        currentIndex: _selectedIndex,
        type: BottomNavigationBarType.fixed, // This is all you need!w1

        onTap: _onItemTapped,
      ),
    );
  }
}
