import 'package:flutter/material.dart';
import 'package:movie_suggestion_app/Pages/Profile/UserFollowingWidget.dart';
import 'package:movie_suggestion_app/internationalization/TJLocalizations.dart';
import 'UserMoviesWidget.dart';
import 'UserFollowersWidget.dart';
import 'UserInfoWidget.dart';

class Profile extends StatefulWidget {
  final int? userID;
  Profile({Key? key, this.userID}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Container(
        color: Theme.of(context).colorScheme.background,
        child: Column(
          children: [
            Expanded(
                child: UserInfoWidget(userID: this.widget.userID), flex: 5),
            Expanded(
                flex: 14,
                child: DefaultTabController(
                    length: 3,
                    child: Scaffold(
                      backgroundColor: Theme.of(context).colorScheme.background,
                      appBar: TabBar(
                        indicatorColor: Theme.of(context).colorScheme.primary,
                        labelColor: Theme.of(context).colorScheme.onSecondary,
                        tabs: [
                          Tab(
                              text: TJLocalizations.of(context)!
                                  .getElement("PROFILE_TAB_TITLE_MOVIES")),
                          Tab(
                              text: TJLocalizations.of(context)!
                                  .getElement("PROFILE_TAB_TITLE_FOLLOWING")),
                          Tab(
                              text: TJLocalizations.of(context)!
                                  .getElement("PROFILE_TAB_TITLE_FOLLOWERS")),
                        ],
                      ),
                      body: Column(children: [
                        Expanded(
                            child: TabBarView(
                          children: [
                            UserMoviesListWidget(userID: this.widget.userID),
                            UserFollowingListWidget(userID: this.widget.userID),
                            UserFollowersListWidget(userID: this.widget.userID)
                          ],
                        ))
                      ]),
                    )))
          ],
        ));
  }
}
