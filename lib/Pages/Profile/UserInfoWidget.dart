import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:movie_suggestion_app/Pages/Profile/Http/ProfileInfoHttpResponse.dart';
import 'package:movie_suggestion_app/internationalization/TJLocalizations.dart';
import 'package:movie_suggestion_app/utilities/AppState.dart';
import 'package:movie_suggestion_app/utilities/Communications.dart';
import 'package:movie_suggestion_app/Config/Globals.dart';
import 'package:provider/src/provider.dart';
import 'package:shimmer/shimmer.dart';

String buildGetProfileInfoRequestUrl(userID) =>
    "$PROFILE_ENDPOINT/${userID != null ? "$userID" : ""}";
String buildFollowRequestUrl(userID) => "$PROFILE_ENDPOINT/$userID/follow";

class UpdateProfileDetails {
  final String? username;

  UpdateProfileDetails([this.username]);

  @override
  String toString() {
    return "$username";
  }
}

class UserInfoWidget extends StatefulWidget {
  final int? userID;
  UserInfoWidget({Key? key, this.userID}) : super(key: key);

  @override
  _UserInfoWidgetState createState() => _UserInfoWidgetState();
}

class _UserInfoWidgetState extends State<UserInfoWidget> {
  Future<bool> sendUnFollowRequest() {
    return sendDeleteRequest(buildFollowRequestUrl(this.widget.userID), [], {})
        .then((result) {
      switch (result!.statusCode) {
        case 200:
          return true;
      }
      return false;
    }).catchError((err) {
      //Handle response error
      return false;
    });
  }

  Future<bool> sendFollowRequest() {
    return sendPutRequest(buildFollowRequestUrl(this.widget.userID), [], {})
        .then((result) {
      switch (result!.statusCode) {
        case 200:
          return true;
      }
      return false;
    }).catchError((err) {
      //Handle response error
      return false;
    });
  }

  Future<bool> sendProfileInfoUpdate(UpdateProfileDetails details) {
    return sendPostNoHeadersRequest(
            PROFILE_ENDPOINT, [], {"username": details.username})
        .then((result) async {
      return false;
    }).catchError((err) {
      //Handle response error
      return false;
    });
  }

  Future<ProfileInfoHttpResponse?>? getProfileInformation() {
    return sendGetRequest(
            buildGetProfileInfoRequestUrl((this.widget.userID)), [], {})
        .then((res) async {
      if (res != null) {
        switch (res.statusCode) {
          case 200:
            return ProfileInfoHttpResponse.fromMap(
                await getJsonStringFromResponse(res));
        }
        // Handle server side error
      } else {
        //Handle response is null
      }
      return null;
    }).catchError((err) {
      //Handle response error
      return null;
    });
  }

  Future<UpdateProfileDetails?> _showMaterialDialog(
      BuildContext context, String currentUsername) async {
    var accentColor = Colors.black;
    var inputFieldTextStyle = TextStyle(color: accentColor);
    var borderSettings = OutlineInputBorder(
        borderSide: BorderSide(width: 2, color: accentColor),
        borderRadius: BorderRadius.all(Radius.circular(15)));
    var focusedBorderSettings = OutlineInputBorder(
        borderSide: BorderSide(width: 2, color: accentColor),
        borderRadius: BorderRadius.all(Radius.circular(15)));
    final controller = TextEditingController();
    controller.text = currentUsername;
    final usernameField = TextField(
        controller: controller,
        obscureText: false,
        decoration: InputDecoration(
          fillColor: accentColor,
          hintStyle: inputFieldTextStyle,
          labelText: TJLocalizations.of(context)!
              .getElement("PROFILE_EDIT_DIALOG_USERNAME_FIELD"),
          labelStyle: inputFieldTextStyle,
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          focusedBorder: focusedBorderSettings,
          enabledBorder: focusedBorderSettings,
          border: borderSettings,
        ));
    return await showDialog<UpdateProfileDetails>(
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (context, setState) {
            return AlertDialog(
              title: Center(
                  child: Text(TJLocalizations.of(context)!
                      .getElement("PROFILE_EDIT_DIALOG_TITLE"))),
              content: Column(mainAxisSize: MainAxisSize.min, children: [
                Padding(
                    child: usernameField,
                    padding: EdgeInsets.fromLTRB(0, 8, 0, 8)),
              ]),
              actions: <Widget>[
                TextButton(
                    onPressed: () {
                      Navigator.pop(
                          context, new UpdateProfileDetails(controller.text));
                    },
                    child: Text(TJLocalizations.of(context)!
                        .getElement("PROFILE_EDIT_DIALOG_SAVE_BUTTON"))),
                TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text(TJLocalizations.of(context)!
                      .getElement("PROFILE_EDIT_DIALOG_CANCEL_BUTTON")),
                )
              ],
            );
          });
        });
  }

  Widget buildStatWidget(String name, String value) {
    return Expanded(
        flex: 2,
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Text(value),
          Text(
            name,
            style: TextStyle(fontWeight: FontWeight.bold),
          )
        ]));
  }

  Widget buildLoadingStatWidget(String name) {
    return Expanded(
        flex: 2,
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Expanded(
              child: Shimmer.fromColors(
                  baseColor: Colors.transparent,
                  highlightColor: Colors.grey[200]!,
                  child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                          color: Colors.white),
                      height: 20,
                      width: Size.infinite.width))),
          Text(
            name,
            style: TextStyle(fontWeight: FontWeight.bold),
          )
        ]));
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<ProfileInfoHttpResponse?>(
        future:
            getProfileInformation(), // a previously-obtained Future<String> or null
        builder: (BuildContext context,
            AsyncSnapshot<ProfileInfoHttpResponse?> snapshot) {
          return Column(children: [
            Expanded(
                flex: 4,
                child: Padding(
                    padding: EdgeInsets.all(8),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                            flex: 4,
                            child: Column(children: [
                              if (snapshot.hasData) ...[
                                Expanded(
                                    child: CircleAvatar(
                                  backgroundImage: NetworkImage(context
                                          .read<AppState>()
                                          .serverAddress +
                                      PROFILE_IMAGE_URL +
                                      (widget.userID ??
                                              (context.read<AppState>().userID))
                                          .toString()),
                                  radius: 120.0,
                                )),
                                Text(snapshot.data!.username)
                              ] else ...[
                                Expanded(
                                    child: Shimmer.fromColors(
                                        baseColor: Colors.white,
                                        highlightColor: Colors.grey,
                                        child: CircleAvatar(
                                          child: Image.asset(
                                              "assets/images/noavatar.png"),
                                          radius: 120.0,
                                        ))),
                                Shimmer.fromColors(
                                    baseColor: Colors.transparent,
                                    highlightColor: Colors.grey[200]!,
                                    child: Container(
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(12)),
                                            color: Colors.white),
                                        height: 20,
                                        width: Size.infinite.width))
                              ]
                            ])),
                        if (snapshot.hasData) ...[
                          buildStatWidget(
                              TJLocalizations.of(context)!
                                  .getElement("PROFILE_INFO_MOVIES"),
                              snapshot.data!.moviesRated.toString()),
                          buildStatWidget(
                              TJLocalizations.of(context)!
                                  .getElement("PROFILE_INFO_FOLLOWING"),
                              snapshot.data!.following.toString()),
                          buildStatWidget(
                              TJLocalizations.of(context)!
                                  .getElement("PROFILE_INFO_FOLLOWERS"),
                              snapshot.data!.followers.toString())
                        ] else ...[
                          buildLoadingStatWidget(TJLocalizations.of(context)!
                              .getElement("PROFILE_INFO_MOVIES")),
                          buildLoadingStatWidget(TJLocalizations.of(context)!
                              .getElement("PROFILE_INFO_FOLLOWING")),
                          buildLoadingStatWidget(TJLocalizations.of(context)!
                              .getElement("PROFILE_INFO_FOLLOWERS")),
                        ]
                      ],
                    ))),
            if (snapshot.hasData && snapshot.data != null) ...[
              if (snapshot.data!.isCurrentUser)
                Expanded(
                  flex: 1,
                  child: Padding(
                      padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
                      child: ElevatedButton(
                        style: ButtonStyle(
                            minimumSize:
                                MaterialStateProperty.all(Size.infinite),
                            foregroundColor:
                                MaterialStateProperty.all(Colors.white),
                            backgroundColor:
                                MaterialStateProperty.all(Colors.black)),
                        onPressed: () async {
                          var updatedProfile = await _showMaterialDialog(
                              context, snapshot.data!.username);
                          if (updatedProfile != null) {
                            await sendProfileInfoUpdate(updatedProfile);
                            setState(() {});
                          }
                        },
                        child: Text(TJLocalizations.of(context)!
                            .getElement("PROFILE_INFO_EDIT_PROFILE_BUTTON")),
                      )),
                )
              else
                Expanded(
                  flex: 1,
                  child: Padding(
                      padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
                      child: ElevatedButton(
                        style: ButtonStyle(
                            minimumSize:
                                MaterialStateProperty.all(Size.infinite),
                            foregroundColor:
                                MaterialStateProperty.all(Colors.white),
                            backgroundColor: snapshot.data!.isFollowed
                                ? MaterialStateProperty.all(Colors.red)
                                : MaterialStateProperty.all(Colors.blue)),
                        onPressed: () async {
                          if (snapshot.data!.isFollowed)
                            await sendUnFollowRequest();
                          else
                            await sendFollowRequest();
                          setState(() {});
                        },
                        child: Text(snapshot.data!.isFollowed
                            ? TJLocalizations.of(context)!
                                .getElement("PROFILE_INFO_UNFOLLOW_BUTTON")
                            : TJLocalizations.of(context)!
                                .getElement("PROFILE_INFO_FOLLOW_BUTTON")),
                      )),
                )
            ]
          ]);
        });
  }
}
