import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:movie_suggestion_app/Config/Globals.dart';
import 'package:movie_suggestion_app/Pages/Profile/Http/UserRatedMoviesHttpResponse.dart';
import 'package:movie_suggestion_app/internationalization/TJLocalizations.dart';
import 'package:movie_suggestion_app/utilities/Communications.dart';
import 'package:movie_suggestion_app/widgets/Movie/MovieDisplayDetached.dart';

String buildUserMoviesRequestUrl(int? userID, int offset) => userID == null
    ? "$PROFILE_ENDPOINT/movies/$offset"
    : "$PROFILE_ENDPOINT/$userID/movies/$offset";

class MovieProfileRepository {
  static Future<UserRatedMoviesHttpResponse> fetchMovies(
      int? userID, int offset) {
    return sendGetRequest(buildUserMoviesRequestUrl(userID, offset), [], {})
        .then((result) async {
      switch (result!.statusCode) {
        case 200:
          return UserRatedMoviesHttpResponse.fromList(
              await getJsonStringFromResponse(result));
      }
      return Future.value(UserRatedMoviesHttpResponse.fromList([]));
    }).catchError((err) {
      //Handle response error
      return Future.value(UserRatedMoviesHttpResponse.fromList([]));
    });
  }
}

class UserMoviesListWidget extends StatefulWidget {
  final int? userID;

  UserMoviesListWidget({Key? key, this.userID}) : super(key: key);

  @override
  _UserMoviesListWidgetState createState() => _UserMoviesListWidgetState();
}

class _UserMoviesListWidgetState extends State<UserMoviesListWidget> {
  bool _isLoading = false;
  List<UserRatedMoviesHttpResponseItem> _movies = [];

  void _fetchData(int? userID, int offset) async {
    var res = await MovieProfileRepository.fetchMovies(userID, offset);
    setState(() {
      _movies.addAll(res.items);
      _isLoading = false;
    });
  }

  @override
  void initState() {
    _fetchData(this.widget.userID, 0);
    super.initState();
  }

  Widget _buildMovies() {
    if (_movies.length > 0)
      return ListView.builder(
        itemCount: _movies.length,
        itemBuilder: (context, index) {
          var element = _movies[index];
          return GestureDetector(
            child: Card(
                shadowColor: Colors.red,
                child: SizedBox(
                    height: 128,
                    child: Container(
                      padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(
                                flex: 2,
                                child: Container(
                                  padding: EdgeInsets.zero,
                                  child: FadeInImage(
                                    placeholder: AssetImage(
                                        "assets/images/noposter.png"),
                                    image: element.imageURL == null
                                        ? AssetImage(
                                                "assets/images/noposter.png")
                                            as ImageProvider
                                        : NetworkImage(element.imageURL),
                                    fit: BoxFit.fitHeight,
                                  ),
                                  margin: EdgeInsets.zero,
                                )),
                            Expanded(
                                flex: 8,
                                child: Container(
                                    margin: EdgeInsets.all(16),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        RichText(
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          text: TextSpan(
                                              style: TextStyle(
                                                  color: Colors.black),
                                              text: element.originalTitle),
                                          textScaleFactor: 1.2,
                                        ),
                                        Text(
                                            "${TJLocalizations.of(context)!.getElement("SEARCH_MOVIES_MOVIE_YEAR")} : ${element.startYear}"),
                                        Text(
                                            "${TJLocalizations.of(context)!.getElement("SEARCH_MOVIES_MOVIE_RATING")} : ${element.averageRating}"),
                                        RatingBarIndicator(
                                          itemSize: 20,
                                          rating: (element.ourRating ?? 0) / 2,
                                          direction: Axis.horizontal,
                                          itemCount: 5,
                                          itemPadding: EdgeInsets.symmetric(
                                              horizontal: 1.0),
                                          itemBuilder: (context, _) => Icon(
                                            Icons.star,
                                            color: Colors.amber,
                                          ),
                                        )
                                      ],
                                    )))
                          ]),
                    ))),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        MovieDetachedWidget(movieID: element.movieID)),
              );
              //PRESSED
            },
          );
        },
      );
    else
      return Center(
          child: Padding(
              padding: EdgeInsets.fromLTRB(0, 8, 0, 0),
              child: Text(TJLocalizations.of(context)!
                  .getElement("PROFILE_TAB_MOVIES_NO_RATED"))));
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Expanded(
          child: NotificationListener<ScrollNotification>(
              onNotification: (ScrollNotification scrollInfo) {
                if (scrollInfo is UserScrollNotification) {
                  if (scrollInfo.direction == ScrollDirection.reverse) {
                    if (!_isLoading &&
                        scrollInfo.metrics.pixels ==
                            scrollInfo.metrics.maxScrollExtent) {
                      _fetchData(this.widget.userID, _movies.length);
                      // start loading data
                      setState(() {
                        _isLoading = true;
                      });
                    }
                  }
                }
                return true;
              },
              child: _buildMovies())),
      Container(
        height: _isLoading ? 50.0 : 0,
        color: Colors.transparent,
        child: Center(
          child: new CircularProgressIndicator(),
        ),
      ),
    ]);
  }
}
