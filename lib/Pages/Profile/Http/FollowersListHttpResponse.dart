class FollowersHttpResponseItem {
  int id;
  String username;
  bool isFollowed;

  FollowersHttpResponseItem(
      {required this.id, required this.username, required this.isFollowed});
  factory FollowersHttpResponseItem.fromMap(Map<String, dynamic> json) {
    return new FollowersHttpResponseItem(
        id: json["id"],
        username: json["username"] ?? "0",
        isFollowed: json["is_followed"]);
  }

  @override
  String toString() =>
      """{ id : $id , username : $username isfollowed : $isFollowed""";
}

class FollowersHttpResponse {
  // String? profilePicture;
  List<FollowersHttpResponseItem> items;

  FollowersHttpResponse({required this.items});

  factory FollowersHttpResponse.fromList(List users) {
    List<FollowersHttpResponseItem> items = [];

    users.forEach(
        (element) => items.add(FollowersHttpResponseItem.fromMap(element)));
    return new FollowersHttpResponse(items: items);
  }

  @override
  String toString() => """ List : $items""";
}
