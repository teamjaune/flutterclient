class ProfileInfoHttpResponse {
  int moviesRated;
  int followers;
  int following;
  String username;
  bool isCurrentUser;
  bool isFollowed;
  String? profilePicture;

  ProfileInfoHttpResponse(
      {/*String? profilePicture,*/
      required this.isCurrentUser,
      required this.moviesRated,
      required this.followers,
      required this.following,
      required this.username,
      required this.isFollowed,
      required this.profilePicture});

  factory ProfileInfoHttpResponse.fromMap(Map<String, dynamic> json) {
    return new ProfileInfoHttpResponse(
        isCurrentUser: json["is_current"],
        moviesRated: json["movie_count"] ?? 0,
        followers: json["followers_count"] ?? 0,
        following: json["following_count"] ?? 0,
        username: json["username"] ?? "",
        isFollowed: json["is_followed"],
        profilePicture: json["picture"]);
  }

  Map<String, dynamic> toMap() => {
        "is_current": isCurrentUser,
        "username": username,
        "movie_count": moviesRated,
        "followers_count": followers,
        "following_count": following,
        "is_followed": isFollowed,
        "picture": profilePicture
      };

  @override
  String toString() => """ username : $username
                           is_current : $isCurrentUser
                           movie_count : $moviesRated
                           followers_count : $followers
                           following_count : $following""";
}
