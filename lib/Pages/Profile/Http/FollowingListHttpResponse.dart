class FollowingHttpResponseItem {
  int id;
  String username;
  bool isFollowed;

  FollowingHttpResponseItem(
      {required this.id, required this.username, required this.isFollowed});
  factory FollowingHttpResponseItem.fromMap(Map<String, dynamic> json) {
    return new FollowingHttpResponseItem(
        id: json["id"],
        username: json["username"] ?? "0",
        isFollowed: json["is_followed"]);
  }

  @override
  String toString() =>
      """{ id : $id , username : $username isfollowed : $isFollowed""";
}

class FollowingHttpResponse {
  // String? profilePicture;
  List<FollowingHttpResponseItem> items;

  FollowingHttpResponse({required this.items});

  factory FollowingHttpResponse.fromList(List users) {
    List<FollowingHttpResponseItem> items = [];

    users.forEach(
        (element) => items.add(FollowingHttpResponseItem.fromMap(element)));
    return new FollowingHttpResponse(items: items);
  }

  @override
  String toString() => """ List : $items""";
}
