import 'package:movie_suggestion_app/Config/Globals.dart';
import 'package:movie_suggestion_app/utilities/Communications.dart';

import 'Http/FollowersListHttpResponse.dart';
import 'Http/FollowingListHttpResponse.dart';

String buildUserfollowingRequestUrl(int? userID, int offset) => userID == null
    ? "$PROFILE_ENDPOINT/following/$offset"
    : "$PROFILE_ENDPOINT/$userID/following/$offset";

String buildUserFollowersRequestUrl(int? userID, int offset) => userID == null
    ? "$PROFILE_ENDPOINT/followers/$offset"
    : "$PROFILE_ENDPOINT/$userID/followers/$offset";

class ProfileRepository {
  static Future<FollowingHttpResponse> fetchFollowing(int? userID, int offset) {
    return sendGetRequest(buildUserfollowingRequestUrl(userID, offset), [], {})
        .then((result) async {
      switch (result!.statusCode) {
        case 200:
          return FollowingHttpResponse.fromList(
              await getJsonStringFromResponse(result));
      }
      return Future.value(FollowingHttpResponse.fromList([]));
    }).catchError((err) {
      return Future.value(FollowingHttpResponse.fromList([]));
    });
  }

  static Future<FollowersHttpResponse> fetchFollowers(int? userID, int offset) {
    return sendGetRequest(buildUserFollowersRequestUrl(userID, offset), [], {})
        .then((result) async {
      switch (result!.statusCode) {
        case 200:
          return FollowersHttpResponse.fromList(
              await getJsonStringFromResponse(result));
      }
      return Future.value(FollowersHttpResponse.fromList([]));
    }).catchError((err) {
      //Handle response error
      return Future.value(FollowersHttpResponse.fromList([]));
    });
  }
}
