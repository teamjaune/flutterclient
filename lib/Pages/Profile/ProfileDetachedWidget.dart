import 'package:flutter/material.dart';
import 'Profile.dart';

class ProfileDetachedWidget extends StatefulWidget {
  final int? userID;
  ProfileDetachedWidget({Key? key, this.userID}) : super(key: key);

  @override
  _ProfileDetachedWidgetState createState() => _ProfileDetachedWidgetState();
}

class _ProfileDetachedWidgetState extends State<ProfileDetachedWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              icon: Icon(Icons.arrow_back)),
          centerTitle: true,
          title: Text("Movie Suggester"),
        ),
        body: Center(
          child: Profile(userID: this.widget.userID),
        ));
  }
}
