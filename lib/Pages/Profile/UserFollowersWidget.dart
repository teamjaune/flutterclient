import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:movie_suggestion_app/Config/Globals.dart';
import 'package:movie_suggestion_app/Pages/Profile/Http/FollowersListHttpResponse.dart';
import 'package:movie_suggestion_app/Pages/Profile/ProfileRepository.dart';
import 'package:movie_suggestion_app/internationalization/TJLocalizations.dart';
import 'package:movie_suggestion_app/utilities/AppState.dart';
import 'package:provider/src/provider.dart';
import 'ProfileDetachedWidget.dart';

class UserFollowersListWidget extends StatefulWidget {
  final int? userID;

  UserFollowersListWidget({Key? key, this.userID}) : super(key: key);

  @override
  _UserFollowersListWidgetState createState() =>
      _UserFollowersListWidgetState();
}

class _UserFollowersListWidgetState extends State<UserFollowersListWidget> {
  bool _isLoading = false;
  List<FollowersHttpResponseItem> _users = [];

  void _fetchData(int? userID, int offset) async {
    var res = await ProfileRepository.fetchFollowers(userID, offset);
    setState(() {
      _users.addAll(res.items);
      _isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    _fetchData(this.widget.userID, 0);
  }

  Widget _buildUsersList() {
    if (_users.length > 0)
      return ListView.builder(
        itemCount: _users.length,
        itemBuilder: (context, index) {
          var element = _users[index];
          return GestureDetector(
            child: Card(
                shadowColor: Colors.red,
                child: SizedBox(
                    height: 94,
                    child: Container(
                      padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(
                                flex: 2,
                                child: Container(
                                  padding: EdgeInsets.zero,
                                  child: CircleAvatar(
                                    backgroundImage: NetworkImage(
                                        context.read<AppState>().serverAddress +
                                            PROFILE_IMAGE_URL +
                                            (element.id).toString()),
                                    radius: 120.0,
                                  ),
                                  margin: EdgeInsets.zero,
                                )),
                            Expanded(
                                flex: 8,
                                child: Container(
                                    margin: EdgeInsets.all(16),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        RichText(
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                          text: TextSpan(
                                              style: TextStyle(
                                                  color: Colors.black),
                                              text: element.username),
                                          textScaleFactor: 1.2,
                                        )
                                      ],
                                    )))
                          ]),
                    ))),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        ProfileDetachedWidget(userID: element.id)),
              );
              //PRESSED
            },
          );
        },
      );
    else
      return Center(
          child: Padding(
              padding: EdgeInsets.fromLTRB(0, 8, 0, 0),
              child: Text(TJLocalizations.of(context)!
                  .getElement("PROFILE_TAB_FOLLOWERS_NO_FOLLOWERS"))));
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Expanded(
          child: NotificationListener<ScrollNotification>(
              onNotification: (ScrollNotification scrollInfo) {
                if (scrollInfo is UserScrollNotification) {
                  if (scrollInfo.direction == ScrollDirection.reverse) {
                    if (!_isLoading &&
                        scrollInfo.metrics.pixels ==
                            scrollInfo.metrics.maxScrollExtent) {
                      _fetchData(this.widget.userID, _users.length);
                      // start loading data
                      setState(() {
                        _isLoading = true;
                      });
                    }
                  }
                }
                return true;
              },
              child: _buildUsersList())),
      Container(
        height: _isLoading ? 50.0 : 0,
        color: Colors.transparent,
        child: Center(
          child: new CircularProgressIndicator(),
        ),
      ),
    ]);
  }
}
