import 'package:flutter/material.dart';

import 'PersonDisplay.dart';

class PersonDetachedWidget extends StatefulWidget {
  final String? personID;
  PersonDetachedWidget({Key? key, this.personID}) : super(key: key);

  @override
  _PersonDetachedWidgetState createState() => _PersonDetachedWidgetState();
}

class _PersonDetachedWidgetState extends State<PersonDetachedWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              icon: Icon(Icons.arrow_back)),
          centerTitle: true,
          title: Text("Movie Suggester"),
        ),
        body: Center(
          child: PersonDisplay(personID: this.widget.personID),
        ));
  }
}
