import 'package:flutter/material.dart';
import 'package:movie_suggestion_app/HttpObjects/Person/PersonInfoHttpResponse.dart';
import 'package:movie_suggestion_app/HttpObjects/Person/PersonMoviesHttpResponse.dart';
import 'package:movie_suggestion_app/internationalization/TJLocalizations.dart';
import 'package:movie_suggestion_app/utilities/Communications.dart';
import 'package:movie_suggestion_app/Config/Globals.dart';
import 'package:movie_suggestion_app/widgets/Movie/MovieDisplayDetached.dart';

String buildPersonInfoRequestUrl(personID) => "$PEOPLE_ENDPOINT/$personID";

class PersonDisplay extends StatefulWidget {
  final String? personID;
  PersonDisplay({Key? key, this.personID}) : super(key: key);

  @override
  _PersonDisplayState createState() => _PersonDisplayState();
}

class _PersonDisplayState extends State<PersonDisplay> {
  Future<PersonInfoHttpResponse?> getPersonInformation(String? personID) async {
    return sendGetRequest(buildPersonInfoRequestUrl(personID), [], {})
        .then((res) async {
      if (res != null) {
        switch (res.statusCode) {
          case 200:
            return PersonInfoHttpResponse.fromMap(
                await getJsonStringFromResponse(res));
        }
        // Handle server side error
      } else {
        //Handle response is null
      }
      return null;
    }).catchError((err) {
      //Handle response error
      return null;
    });
  }

  Widget buildpersonTitle(PersonInfoHttpResponse person) {
    return Padding(
        padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
        child: Row(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
          Flexible(
              flex: 4,
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: FadeInImage(
                    placeholder: AssetImage("assets/images/noposter.png"),
                    image: person.imageURL == null
                        ? AssetImage("assets/images/noposter.png")
                            as ImageProvider
                        : NetworkImage(person.imageURL!),
                    fit: BoxFit.fitHeight,
                  ))),
          Padding(padding: EdgeInsets.all(8)),
          Expanded(
              flex: 6,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    person.name,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    textScaleFactor: 1.4,
                  ),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Birth Year"),
                              Text("Deathg year"),
                            ]),
                        Column(children: [
                          Text(":"),
                          Text(":"),
                        ]),
                        Column(children: [
                          Text("${person.birthYear}"),
                          Text("${person.deathYear}"),
                        ])
                      ])
                ],
              ))
        ]));
  }

  Widget buildMoviesList(BuildContext context, PersonMoviesHttpResponse data) {
    List<PersonMoviesHttpResponseItem> movies = data.items;
    if (movies.length > 0) {
      return ListView.builder(
        itemCount: movies.length,
        itemBuilder: (context, index) {
          var element = movies[index];
          return GestureDetector(
            child: Card(
                shadowColor: Colors.red,
                child: SizedBox(
                    height: 128,
                    child: Container(
                      padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(
                                flex: 2,
                                child: Container(
                                  padding: EdgeInsets.zero,
                                  child: FadeInImage(
                                    placeholder: AssetImage(
                                        "assets/images/noposter.png"),
                                    image: element.imageURL == null
                                        ? AssetImage(
                                                "assets/images/noposter.png")
                                            as ImageProvider
                                        : NetworkImage(element.imageURL),
                                    fit: BoxFit.fitHeight,
                                  ),
                                  margin: EdgeInsets.zero,
                                )),
                            Expanded(
                                flex: 8,
                                child: Container(
                                    margin: EdgeInsets.all(16),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        RichText(
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          text: TextSpan(
                                              style: TextStyle(
                                                  color: Colors.black),
                                              text: element.originalTitle),
                                          textScaleFactor: 1.2,
                                        ),
                                        Text(
                                            "${TJLocalizations.of(context)!.getElement("SEARCH_MOVIES_MOVIE_YEAR")} : ${element.startYear}"),
                                        Text(
                                            "${TJLocalizations.of(context)!.getElement("SEARCH_MOVIES_MOVIE_RATING")} : ${element.endYear}"),
                                        Text(
                                            "${TJLocalizations.of(context)!.getElement("SEARCH_MOVIES_MOVIE_TYPE")} : ${element.type}")
                                      ],
                                    )))
                          ]),
                    ))),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        MovieDetachedWidget(movieID: element.movieID)),
              );
            },
          );
        },
      );
    }

    return Center(
        child: Padding(
            padding: EdgeInsets.fromLTRB(0, 8, 0, 0),
            child: Text(
                TJLocalizations.of(context)!.getElement("SEARCH_NO_RESULT"))));
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<PersonInfoHttpResponse?>(
        future: getPersonInformation(this.widget.personID),
        builder: (BuildContext context,
            AsyncSnapshot<PersonInfoHttpResponse?> snapshot) {
          if (snapshot.hasData) {
            var personData = snapshot.data!;
            return SingleChildScrollView(
                child: ConstrainedBox(
                    constraints: BoxConstraints(
                        maxHeight: MediaQuery.of(context).size.height * 0.9),
                    child: Column(children: [
                      SizedBox(
                          height: MediaQuery.of(context).size.height * 0.25,
                          child: buildpersonTitle(personData)),
                      Expanded(
                          child: buildMoviesList(context, personData.movies))
                    ])));
          } else if (snapshot.hasError)
            print("Error");
          else
            return SizedBox(
              child: Center(child: CircularProgressIndicator()),
              width: 60,
              height: 60,
            );

          return Text("Hello");
        });
  }
}
