import 'package:flutter/material.dart';
import 'package:movie_suggestion_app/internationalization/TJLocalizations.dart';
import 'package:movie_suggestion_app/utilities/Debouncer.dart';
import 'package:movie_suggestion_app/widgets/NumberPicker/numberpicker.dart';

class YearFilter extends StatefulWidget {
  final Function callBack;
  const YearFilter({Key? key, required this.callBack}) : super(key: key);

  @override
  State createState() => YearFilterState();
}

class YearFilterState extends State<YearFilter> {
  int _currentFromValue = 1888;
  int _currentToValue = 2050;

  final Debouncer _debounce = Debouncer(Duration(milliseconds: 400));

  void sendEvent() {
    _debounce(() => {widget.callBack(_currentFromValue, _currentToValue)});
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(TJLocalizations.of(context)!
                .getElement("SEARCH_FILTER_YEAR_TITLE")),
            NumberPicker(
              itemCount: 1,
              value: _currentFromValue,
              minValue: 1888,
              maxValue: 2050,
              onChanged: (val) => setState(() {
                _currentFromValue = val;
                sendEvent();
              }),
            ),
            Text(TJLocalizations.of(context)!
                .getElement("SEARCH_FILTER_YEAR_TO")),
            NumberPicker(
              itemCount: 1,
              value: _currentToValue,
              minValue: 1888,
              maxValue: 2050,
              onChanged: (val) => setState(() {
                _currentToValue = val;
                sendEvent();
              }),
            ),
          ],
        ));
  }
}
