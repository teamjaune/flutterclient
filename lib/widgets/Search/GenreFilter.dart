import 'package:flutter/material.dart';

final List<String> genreList = <String>[
  "Sport",
  "History",
  "Musical",
  "Music",
  "Fantasy",
  "Horror",
  "Drama",
  "Action",
  "Biography",
  "Talk-Show",
  "Thriller",
  "Sci-Fi",
  "Comedy",
  "War",
  "Animation",
  "Game-Show",
  "Romance",
  "Documentary",
  "Reality-TV",
  "Mystery",
  "Film-Noir",
  "Short",
  "Adult",
  "News",
  "Western",
  "Adventure",
  "Family",
  "Crime"
];

class GenreFilter extends StatefulWidget {
  final Function callBack;

  GenreFilter({Key? key, required this.callBack}) : super(key: key);

  @override
  State createState() => GenreFilterState();
}

class GenreFilterState extends State<GenreFilter> {
  final List<String> _filters = [];

  Iterable<Widget> get actorWidgets sync* {
    for (final String genre in genreList) {
      yield Padding(
        padding: const EdgeInsets.all(4.0),
        child: FilterChip(
          label: Text(genre),
          selected: !_filters.contains(genre),
          onSelected: (bool value) {
            setState(() {
              if (value) {
                _filters.removeWhere((String name) {
                  return name == genre;
                });
              } else {
                _filters.add(genre);
              }
              widget.callBack(_filters);
            });
          },
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Wrap(
              children: actorWidgets.toList(),
            ),
          ],
        ));
  }
}
