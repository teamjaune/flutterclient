import 'package:flutter/material.dart';

class ReadMore extends StatefulWidget {
  final String? title;

  final String text;
  ReadMore({Key? key, required this.text, this.title}) : super(key: key);

  @override
  _ReadMoreState createState() => _ReadMoreState();
}

class _ReadMoreState extends State<ReadMore> {
  bool isExpanded = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (this.widget.title != null)
          Text(
            this.widget.title!,
            textScaleFactor: 1.4,
          ),
        Padding(padding: EdgeInsets.all(4)),
        Text(
          this.widget.text,
          maxLines: isExpanded ? 100 : 2,
          overflow: TextOverflow.ellipsis,
        ),
        TextButton.icon(
            style: ButtonStyle(
                padding: MaterialStateProperty.all(EdgeInsets.zero),
                overlayColor: MaterialStateProperty.all(Colors.transparent),
                backgroundColor: MaterialStateProperty.all(Colors.transparent)),
            label: isExpanded ? Text("Less") : Text("more"),
            icon: isExpanded
                ? Icon(Icons.arrow_drop_up_outlined)
                : Icon(Icons.arrow_drop_down_outlined),
            onPressed: () {
              setState(() {
                isExpanded = !isExpanded;
              });
            })
      ],
    );
  }
}
