import 'package:flutter/material.dart';

import 'MovieDisplay.dart';

class MovieDetachedWidget extends StatefulWidget {
  final String? movieID;
  MovieDetachedWidget({Key? key, this.movieID}) : super(key: key);

  @override
  _ProfileDetachedWidgetState createState() => _ProfileDetachedWidgetState();
}

class _ProfileDetachedWidgetState extends State<MovieDetachedWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              icon: Icon(Icons.arrow_back)),
          centerTitle: true,
          title: Text("Movie Suggester"),
        ),
        body: Center(
          child: MovieDisplay(movieID: this.widget.movieID),
        ));
  }
}
