import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:movie_suggestion_app/HttpObjects/Movie/MovieInfoHttpResponse.dart';
import 'package:movie_suggestion_app/utilities/Communications.dart';
import 'package:movie_suggestion_app/Config/Globals.dart';
import 'package:movie_suggestion_app/widgets/Person/PersonDisplayDetached.dart';
import 'package:movie_suggestion_app/widgets/ReadMore.dart';

String buildUserMoviesRequestUrl(movieID) => "$MOVIES_ENDPOINT/$movieID";

class MovieDisplay extends StatefulWidget {
  final String? movieID;
  MovieDisplay({Key? key, this.movieID}) : super(key: key);

  @override
  _MovieDisplayState createState() => _MovieDisplayState();
}

class _MovieDisplayState extends State<MovieDisplay> {
  Future<MovieInfoHttpResponse?> getMovieInformation(String? movieId) async {
    return sendGetRequest(
            buildUserMoviesRequestUrl((this.widget.movieID)), [], {})
        .then((res) async {
      if (res != null) {
        switch (res.statusCode) {
          case 200:
            return MovieInfoHttpResponse.fromMap(
                await getJsonStringFromResponse(res));
        }
        // Handle server side error
      } else {
        //Handle response is null
      }
      return null;
    }).catchError((err) {
      //Handle response error
      return null;
    });
  }

  Widget buildMovieTitle(MovieInfoHttpResponse movie) {
    return Padding(
        padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
        child: Row(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
          Flexible(
              flex: 4,
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: FadeInImage(
                    placeholder: AssetImage("assets/images/noposter.png"),
                    image: movie.imageURL.isEmpty
                        ? AssetImage("assets/images/noposter.png")
                            as ImageProvider
                        : NetworkImage(movie.imageURL),
                    fit: BoxFit.fitHeight,
                  ))),
          Padding(padding: EdgeInsets.all(8)),
          Expanded(
              flex: 6,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Center(child: Text("IMDB Rating")),
                  Center(
                      child: RatingBarIndicator(
                    itemSize: MediaQuery.of(context).size.width * 0.09,
                    rating: movie.averageRating / 2,
                    direction: Axis.horizontal,
                    itemCount: 5,
                    itemPadding: EdgeInsets.symmetric(horizontal: 1.0),
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                  )),
                  Center(child: Text("Average Rating")),
                  Center(
                      child: RatingBarIndicator(
                          itemSize: MediaQuery.of(context).size.width * 0.09,
                          rating: (movie.ourAverageRating ?? 0) / 2,
                          direction: Axis.horizontal,
                          itemCount: 5,
                          itemPadding: EdgeInsets.symmetric(horizontal: 1.0),
                          itemBuilder: (context, _) => Icon(
                                Icons.star,
                                color: Colors.amber,
                              ))),
                  Center(child: Text("Your Rating")),
                  Center(
                      child: RatingBarIndicator(
                          itemSize: MediaQuery.of(context).size.width * 0.09,
                          rating: (movie.rating ?? 0) / 2,
                          direction: Axis.horizontal,
                          itemCount: 5,
                          itemPadding: EdgeInsets.symmetric(horizontal: 1.0),
                          itemBuilder: (context, _) => Icon(
                                Icons.star,
                                color: Colors.amber,
                              )))
                ],
              ))
        ]));
  }

  Widget buildSynopsis(MovieInfoHttpResponse movie) {
    return Padding(
        padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
        child: ReadMore(title: "Story Line", text: movie.synopsis));
  }

  Widget buildGenres(MovieInfoHttpResponse movieData) => Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Genres",
              textScaleFactor: 1.4,
            ),
            SizedBox(
                height: MediaQuery.of(context).size.height * 0.05,
                child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Wrap(
                        spacing: 8,
                        alignment: WrapAlignment.spaceEvenly,
                        children:
                            List.from(movieData.genres!.map((genre) => Chip(
                                  label: Text(genre),
                                ))))))
          ]);

  Widget buildKeywords(MovieInfoHttpResponse movieData) => Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Keywords",
              textScaleFactor: 1.4,
            ),
            SizedBox(
                height: MediaQuery.of(context).size.height * 0.05,
                child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Wrap(
                        spacing: 8,
                        alignment: WrapAlignment.spaceEvenly,
                        children:
                            List.from(movieData.keywords!.map((keyword) => Chip(
                                  label: Text(keyword),
                                ))))))
          ]);
  Widget buildProductionCompanies(MovieInfoHttpResponse movieData) => Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Production Companies",
              textScaleFactor: 1.4,
            ),
            SizedBox(
                height: MediaQuery.of(context).size.height * 0.05,
                child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Wrap(
                        spacing: 8,
                        alignment: WrapAlignment.spaceEvenly,
                        children: List.from(
                            movieData.productionCompanies.map((company) => Chip(
                                  label: Text(company.name),
                                ))))))
          ]);

  Widget buildCrewList(MovieInfoHttpResponse movieData) =>
      SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Wrap(
            children: List.from(movieData.crew.map((person) => GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            PersonDetachedWidget(personID: person.personID)),
                  );
                },
                child: SizedBox(
                    width: MediaQuery.of(context).size.width * 0.25,
                    child: Column(
                      children: [
                        SizedBox(
                            width: 64,
                            child: ClipRRect(
                                borderRadius: BorderRadius.circular(12.0),
                                child: FadeInImage(
                                  placeholder:
                                      AssetImage("assets/images/noposter.png"),
                                  image: person.imageURL == null
                                      ? AssetImage("assets/images/noposter.png")
                                          as ImageProvider
                                      : NetworkImage(person.imageURL!),
                                  fit: BoxFit.fitHeight,
                                ))),
                        Text(
                          person.name,
                          textAlign: TextAlign.center,
                        )
                      ],
                    ))))),
          ));

  String titleBuilder(MovieInfoHttpResponse movieData) {
    var type = (typeEnumToString[movieData.type] ?? "");
    var runTime = "${movieData.runtimeMinutes} Min";

    if (movieData.type == MovieType.TV_SERIES || movieData.endYear != null) {
      return (type.isNotEmpty ? type + " · " : "") +
          "${movieData.startYear}-${movieData.endYear ?? ""}" +
          " · " +
          runTime;
    } else {
      return (type.isNotEmpty ? type + " · " : "") +
          "${movieData.startYear}" +
          " · " +
          runTime;
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<MovieInfoHttpResponse?>(
        future: getMovieInformation(this.widget.movieID),
        builder: (BuildContext context,
            AsyncSnapshot<MovieInfoHttpResponse?> snapshot) {
          if (snapshot.hasData) {
            var movieData = snapshot.data!;
            return SizedBox(
                height: MediaQuery.of(context).size.height,
                child: SingleChildScrollView(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                      Padding(
                          padding: EdgeInsets.fromLTRB(16, 8, 16, 0),
                          child: Center(
                              child: Text(
                            movieData.originalTitle,
                            textAlign: TextAlign.center,
                            maxLines: 5,
                            overflow: TextOverflow.ellipsis,
                            textScaleFactor: 1.4,
                          ))),
                      Padding(
                          padding: EdgeInsets.fromLTRB(16, 8, 16, 0),
                          child: Text(
                            titleBuilder(movieData),
                            textAlign: TextAlign.center,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            textScaleFactor: 1,
                          )),
                      SizedBox(
                          height: MediaQuery.of(context).size.height * 0.25,
                          child: buildMovieTitle(movieData)),
                      Divider(),
                      if (movieData.genres != null &&
                          movieData.genres!.length > 0) ...{
                        Padding(
                            padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                            child: buildGenres(movieData)),
                        Divider(),
                      },
                      buildSynopsis(movieData),
                      Divider(),
                      Padding(
                          padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
                          child: buildCrewList(movieData)),
                      if (movieData.productionCompanies.length > 0) ...{
                        Divider(),
                        Padding(
                            padding: EdgeInsets.fromLTRB(16, 0, 16, 8),
                            child: buildProductionCompanies(movieData)),
                      },
                      if (movieData.keywords != null &&
                          movieData.keywords!.length > 0) ...{
                        Divider(),
                        Padding(
                            padding: EdgeInsets.fromLTRB(16, 0, 16, 8),
                            child: buildKeywords(movieData)),
                      }
                    ])));
          } else if (snapshot.hasError)
            print("Error");
          else
            return SizedBox(
              child: Center(child: CircularProgressIndicator()),
              width: 60,
              height: 60,
            );

          return Text("Hello");
        });
  }
}
