import 'package:flutter/material.dart';

class TextDivider extends StatelessWidget {
  final String text;

  TextDivider(this.text);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
            child: Divider(
          color: Colors.black,
        )),
        SizedBox(width: 16),
        Text(text),
        SizedBox(width: 16),
        Expanded(
            child: Divider(
          color: Colors.black,
        )),
      ],
    );
  }
}
