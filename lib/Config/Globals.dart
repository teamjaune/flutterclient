// ignore_for_file: non_constant_identifier_names

library flutterclient.globals;

//BASE
final String PROTECTED_END_POINTS = "/api/private";

final String LOGIN_ENDPOINT = "/api/public/login";

final String PROFILE_IMAGE_URL = "/api/public/images/profile/";
//BASE
//SEARCH
final String SEARCH_ENDPOINT = PROTECTED_END_POINTS + "/Search";
final String SEARCH_ENDPOINT_MOVIE = PROTECTED_END_POINTS + "/search/movie";
final String SEARCH_ENDPOINT_PEOPLE = PROTECTED_END_POINTS + "/search/crew";
final String SEARCH_ENDPOINT_COMPANY =
    PROTECTED_END_POINTS + "/search/companies";
final String SEARCH_ENDPOINT_USERS = PROTECTED_END_POINTS + "/search/user";

//USERz
final String USER_ENDPOINT = PROTECTED_END_POINTS + "/user";
final String PROFILE_ENDPOINT = USER_ENDPOINT;

//SUGGESTIONS
final String SUGGESTIONS_LIST = PROTECTED_END_POINTS + "/Suggestions";

//MOVIES
final String MOVIES_ENDPOINT = PROTECTED_END_POINTS + "/movie";
final String PEOPLE_ENDPOINT = PROTECTED_END_POINTS + "/crew";

String getUrl(String endPoint) {
  return PROTECTED_END_POINTS + endPoint;
}
