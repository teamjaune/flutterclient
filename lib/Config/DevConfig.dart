import 'BaseConfig.dart';

class DevConfig implements BaseConfig {
  String get apiHost => "http://192.168.1.48:8080";

  set apiHost(String? apiHost) {
    this.apiHost = apiHost;
  }

  bool get reportErrors => false;

  bool get trackEvents => false;

  bool get useHttps => false;
}
