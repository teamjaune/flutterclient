abstract class BaseConfig {
  String get apiHost;
  set apiHost(String? apiHost) {
    this.apiHost = apiHost;
  }

  bool get useHttps;
  bool get trackEvents;
  bool get reportErrors;
}
