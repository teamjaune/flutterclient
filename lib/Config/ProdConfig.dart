import 'BaseConfig.dart';

class ProdConfig implements BaseConfig {
  String get apiHost => "https://api.teamjaune.xyz";

  set apiHost(String? apiHost) {
    this.apiHost = apiHost;
  }

  bool get reportErrors => true;

  bool get trackEvents => false;

  bool get useHttps => true;
}
