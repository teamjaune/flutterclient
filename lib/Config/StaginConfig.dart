import 'BaseConfig.dart';

class StagingConfig implements BaseConfig {
  String get apiHost => "https://staging.teamjaune.xyz";
  set apiHost(String? apiHost) {
    this.apiHost = apiHost;
  }

  bool get reportErrors => true;

  bool get trackEvents => false;

  bool get useHttps => true;
}
