import 'package:devicelocale/devicelocale.dart';
import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// A Class that manages the application state and stores prefrences
class AppState with ChangeNotifier, DiagnosticableTreeMixin {
  Future? _doneLoading;

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  String _serverAddress = "";
  int _userID = -1;

  /// Store the current selected locale
  late String _locale;

  AppState() {
    _doneLoading = _init();
  }
  Future? get initializationDone => _doneLoading;

  Future _init() async {
    await _prefs.then((prefs) async {
      this._locale = prefs.getString("Locale") ??
          (await Devicelocale.currentAsLocale)?.languageCode ??
          "en";
      await prefs.setString('Locale', this._locale);

      this._serverAddress = prefs.getString("ServerAddress") ?? "";
      await prefs.setString('ServerAddress', this._serverAddress);
    });
  }

  String get serverAddress => _serverAddress;
  set serverAddress(String? state) {
    _prefs.then((prefs) => {
          (state == null)
              ? prefs.remove('ServerAddress')
              : prefs.setString('ServerAddress', state)
        });
    _serverAddress = state ?? "";
    notifyListeners();
  }

  String get locale => _locale;
  set locale(String? state) {
    _prefs.then((prefs) => {
          (state == null)
              ? prefs.remove('Locale')
              : prefs.setString('Locale', state)
        });
    _locale = state ?? "en";
    notifyListeners();
  }

  int get userID => _userID;
  set userID(int state) {
    _userID = state;
    notifyListeners();
  }

  void clear() {
    _prefs.then((prefs) async {
      prefs.clear();
      notifyListeners();
    });
  }

  /// Makes `AppState` readable inside the devtools by listing all of its properties
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
  }
}

final appState = AppState();
