library aldaleel.communication;

import 'dart:convert';
import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<HttpClientResponse?> sendPostNoHeadersRequest(String endpoint,
    List<Map<String, String>> headers, Map<String, dynamic> body) async {
  String url =
      ((await SharedPreferences.getInstance()).getString("ServerAddress") ??
              "") +
          endpoint;
  try {
    HttpClient client = new HttpClient();
    var accessToken = await FirebaseAuth.instance.currentUser?.getIdToken();
    HttpClientResponse response =
        await client.postUrl(Uri.parse(url)).then((HttpClientRequest request) {
      if (accessToken != null && accessToken.isNotEmpty)
        request.headers.set('Authorization', accessToken);
      if (headers.isNotEmpty)
        for (Map<String, String> head in headers) {
          head.forEach((key, value) {
            request.headers.add(key, value);
          });
        }
      request.write(jsonEncode(body));
      return request.close();
    }).catchError((onError) {
      print(onError.toString());
    });
    return response;
  } on SocketException {
    throw new UnimplementedError();
  } catch (e) {
    print(e);
  }
  return null;
}

Future<HttpClientResponse?> sendPostRequest(String endpoint,
    List<Map<String, String>> headers, Map<String, dynamic> body) async {
  String url =
      ((await SharedPreferences.getInstance()).getString("ServerAddress") ??
              "") +
          endpoint;

  try {
    HttpClient client = new HttpClient();
    var accessToken = await FirebaseAuth.instance.currentUser?.getIdToken();
    HttpClientResponse response =
        await client.postUrl(Uri.parse(url)).then((HttpClientRequest request) {
      request.headers.set('content-type', 'application/json; charset=utf-8');
      if (accessToken != null && accessToken.isNotEmpty)
        request.headers.set('Authorization', accessToken);
      if (headers.isNotEmpty)
        for (Map<String, String> head in headers) {
          head.forEach((key, value) {
            request.headers.add(key, value);
          });
        }
      request.write(jsonEncode(body));
      return request.close();
    }).catchError((onError) {
      print(onError.toString());
    });
    return response;
  } on SocketException {
    throw new UnimplementedError();
  } catch (e) {
    print(e);
  }
  return null;
}

Future<HttpClientResponse?> sendGetRequest(String endpoint,
    List<Map<String, String>> headers, Map<String, dynamic> query) async {
  String url =
      ((await SharedPreferences.getInstance()).getString("ServerAddress") ??
              "") +
          endpoint;
  try {
    HttpClient client = new HttpClient();
    var accessToken = await FirebaseAuth.instance.currentUser?.getIdToken();

    var uri = Uri.parse(url);
    if (query.length > 0) uri.replace(queryParameters: query);
    HttpClientResponse response =
        await client.getUrl(uri).then((HttpClientRequest request) {
      if (accessToken != null && accessToken.isNotEmpty)
        request.headers.set('Authorization', accessToken);
      request.headers.set('content-type', 'application/json; charset=utf-8');
      if (headers.isNotEmpty)
        for (Map<String, String> head in headers) {
          head.forEach((key, value) {
            request.headers.add(key, value);
          });
        }
      return request.close();
    });

    return response;
  } on SocketException {
    throw new UnimplementedError();
  } catch (e) {
    print(e);
  }
  return null;
}

Future<HttpClientResponse?> sendPutRequest(String endpoint,
    List<Map<String, String>> headers, Map<String, dynamic> query) async {
  String url =
      ((await SharedPreferences.getInstance()).getString("ServerAddress") ??
              "") +
          endpoint;
  try {
    HttpClient client = new HttpClient();
    var accessToken = await FirebaseAuth.instance.currentUser?.getIdToken();
    if (Uri.parse(url).host == "10.0.2.2")
      client.badCertificateCallback =
          ((X509Certificate cert, String host, int port) => true);

    var uri = Uri.parse(url);
    if (query.length > 0) uri.replace(queryParameters: query);
    HttpClientResponse response =
        await client.putUrl(uri).then((HttpClientRequest request) {
      if (accessToken != null && accessToken.isNotEmpty)
        request.headers.set('Authorization', accessToken);
      request.headers.set('content-type', 'application/json; charset=utf-8');
      if (headers.isNotEmpty)
        for (Map<String, String> head in headers) {
          head.forEach((key, value) {
            request.headers.add(key, value);
          });
        }
      return request.close();
    });

    return response;
  } on SocketException {
    throw new UnimplementedError();
  } catch (e) {
    print(e);
  }
  return null;
}

Future<HttpClientResponse?> sendDeleteRequest(String endpoint,
    List<Map<String, String>> headers, Map<String, dynamic> query) async {
  String url =
      ((await SharedPreferences.getInstance()).getString("ServerAddress") ??
              "") +
          endpoint;
  try {
    HttpClient client = new HttpClient();
    var accessToken = await FirebaseAuth.instance.currentUser?.getIdToken();
    if (Uri.parse(url).host == "10.0.2.2")
      client.badCertificateCallback =
          ((X509Certificate cert, String host, int port) => true);

    var uri = Uri.parse(url);
    if (query.length > 0) uri.replace(queryParameters: query);
    HttpClientResponse response =
        await client.deleteUrl(uri).then((HttpClientRequest request) {
      if (accessToken != null && accessToken.isNotEmpty)
        request.headers.set('Authorization', accessToken);
      request.headers.set('content-type', 'application/json; charset=utf-8');
      if (headers.isNotEmpty)
        for (Map<String, String> head in headers) {
          head.forEach((key, value) {
            request.headers.add(key, value);
          });
        }
      return request.close();
    });

    return response;
  } on SocketException {
    throw new UnimplementedError();
  } catch (e) {
    print(e);
  }
  return null;
}

Future<dynamic> getJsonStringFromResponse(HttpClientResponse response) async {
  return jsonDecode(await response.transform(utf8.decoder).join());
}
