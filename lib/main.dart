import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:movie_suggestion_app/Authentication/AuthenticationPage.dart';
import 'package:movie_suggestion_app/Config/Globals.dart';
import 'package:movie_suggestion_app/utilities/AuthenticationProvider.dart';
import 'package:movie_suggestion_app/utilities/Communications.dart';
import 'package:provider/provider.dart';
import 'Themes/LightTheme.dart';
import 'internationalization/ErrorLocalizations.dart';
import 'internationalization/TJLocalizations.dart';
import 'utilities/AppState.dart';
import 'Config/Environment.dart';
import 'Pages/PagesHandler.dart';
import 'package:logger/logger.dart';

var logger = Logger(
  printer: PrettyPrinter(),
);
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  //Initilize environment
  const String environment = String.fromEnvironment(
    'ENVIRONMENT',
    defaultValue: Environment.DEV,
  );

  Environment().initConfig(environment);

  //Initilize Global App State
  var stateHolder = AppState();
  await stateHolder.initializationDone;

  //Initilize Firebase
  await Firebase.initializeApp();
  runApp(App(stateHolder));
}

class App extends StatelessWidget {
  final AppState stateHolder;
  App(this.stateHolder) {
    FirebaseAuth.instance.userChanges().listen((User? user) {
      if (user != null)
        sendGetRequest(LOGIN_ENDPOINT, [], {}).then((result) async {
          switch (result!.statusCode) {
            case 200:
              stateHolder.userID =
                  (await getJsonStringFromResponse(result) as Map)["userID"];
          }
        }).catchError((err) {
          print(err);
        });
      else
        stateHolder.userID = -1;
    });
  }

  Widget mainAppWidget(BuildContext context) {
    return MaterialApp(
        theme: Themes.lightTheme,
        title: 'Flutter Code Sample for Navigator',
        home: PageHandler(),
        localizationsDelegates: [
          const TJLocalizationsDelegate(),
          const DaleelErrorLocalizationsDelegate(),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate
        ],
        supportedLocales: [const Locale("en"), const Locale("fr")],
        locale: new Locale(context.watch<AppState>().locale));
  }

  Widget loggingIn(BuildContext context) {
    return MaterialApp(
        theme: Themes.lightTheme,
        title: 'Flutter Code Sample for Navigator',
        home: Scaffold(
            body: Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [
                    Colors.yellow[50]!,
                    Colors.red[100]!,
                  ],
                )),
                child: Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                      AnimatedTextKit(
                          repeatForever: true,
                          isRepeatingAnimation: true,
                          animatedTexts: [
                            WavyAnimatedText(
                              'Logging you in...',
                              speed: Duration(milliseconds: 200),
                              textStyle: TextStyle(
                                  fontSize: 32.0, fontWeight: FontWeight.bold),
                            ),
                          ]),
                      TextButton(
                          onPressed: () {
                            FirebaseAuth.instance.signOut();
                          },
                          child: Text("Cancel"))
                    ])))),
        localizationsDelegates: [
          const TJLocalizationsDelegate(),
          const DaleelErrorLocalizationsDelegate(),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate
        ],
        supportedLocales: [const Locale("en"), const Locale("fr")],
        locale: new Locale(context.watch<AppState>().locale));
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          Provider<AuthenticationProvider>(
              create: (_) => AuthenticationProvider(FirebaseAuth.instance)),
          StreamProvider(
            create: (context) =>
                context.read<AuthenticationProvider>().authState,
            initialData: null,
          ),
          ChangeNotifierProvider<AppState>(create: (_) => stateHolder),
        ],
        builder: (context, _) {
          final firebaseUser = context.watch<User?>();
          switch (firebaseUser != null) {
            case true:
              if (context.read<AppState>().userID != -1)
                return mainAppWidget(context);
              else
                return loggingIn(context);

            default:
              return MaterialApp(
                  theme: Themes.lightTheme,
                  darkTheme: Themes.lightTheme,
                  home: AuthenticationPage(),
                  localizationsDelegates: [
                    const TJLocalizationsDelegate(),
                    const DaleelErrorLocalizationsDelegate(),
                    GlobalMaterialLocalizations.delegate,
                    GlobalWidgetsLocalizations.delegate
                  ],
                  supportedLocales: [const Locale("en"), const Locale("fr")],
                  locale: new Locale(context.watch<AppState>().locale));
          }
        });
  }
}
